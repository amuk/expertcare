$(document).ready(function(){
	var patientId=sessionStorage.getItem('PatientId');
	var medName=[];
	var presTime=[];
	jQuery('.input-group.date').datepicker({
		autoclose:true,
		todayHighlight:true	
	});
		var $symptom=$("#selectSymAppointment").selectize({
	    delimiter: ',',
	    persist: false,
	    placeholder:"Select or Add Symptoms",
	    labelField: 'text',
		searchField: 'text',
		 options: [
			          {value: 'Anamemia', text: 'Anamemia'},
			          {value:'Low BP', text: 'Low BP'},
			          {value:'Nausea', text: 'Nausea'},
			          {value:'Headache', text: 'Headache'},
			          {value: 'Vomiting', text: 'Vomiting'},
			          {value:'Bodyache', text: 'Bodyache'}
			      ],
		sortField: {
	        field: 'text',
	        direction: 'asc',
	    },
	    create: function(input) {
	        return {
	            value: input,
	            text: input
	        };}
	});
	$("#addPresAppointment").click(function () {
		var $table=document.getElementById("addPrescriptionAppointment");
		var rowCount=$table.rows.length;
		 $('#addPrescriptionAppointment').append('<tr id="pres'+(rowCount)+'"></tr>');
		 $('#pres'+rowCount).html("<td style='min-width:100px;' class='col-md-2 col-xs-2'><select id='medName"+rowCount+"'></select></td>" +
	    		"<td style='min-width:100px'><input  id='dosage"+rowCount+"' type='text' placeholder='Dosage'  class='form-control input-md col-xs-2 col-md-2'></td>" +
	    	    "<td style='min-width:100px' class='col-md-3 col-xs-3'><div class='input-group date'><input id='startDate"+rowCount+"' type='text' placeholder='Start Date'  class='form-control'></div></td>" +
	    	    "<td style='min-width:100px'  class='col-md-3 col-xs-3'><div class='input-group date'><input  id='endDate"+rowCount+"' type='text' placeholder='End Date'  class='form-control'></div></td>" +
		    	"<td style='min-width:100px' class='col-md-2 col-xs-2'><select id='time"+rowCount+"'></select></td>");
       $("#startDate"+rowCount).datepicker({
			autoclose:true,
			todayHighlight:true	
		});
       $("#endDate"+rowCount).datepicker({
			autoclose:true,
			todayHighlight:true	
		});
        medName[rowCount]=$("#medName"+rowCount).selectize({
    		create: true,
    		valueField: 'id',
    	    labelField: 'title',
    		searchField: 'title',
    		placeholder:"Select/Add Medicine",
    		options: [
    		          {id:'med1', title: 'med1'},
    		          {id:'med2', title: 'med2'},
    		          {id:'med3', title: 'med3'},
    		          {id:'med4', title: 'med4'},
    		          {id:'med5', title: 'med5'},
    		          {id:'med6', title: 'med6'},
    		          
    		      ],
    		sortField: {
    	        field: 'title',
    	        direction: 'asc'
    	    }
    	});
       presTime[rowCount]=$("#time"+rowCount).selectize({
    		create: true,
    		valueField: 'id',
    		maxItems: 3,
    	    labelField: 'title',
    		searchField: 'title',
    		placeholder:"Select Time",
    		options: [
    		          {id: 5, title: 'Morning'},
    		          {id: 6, title: 'Afternoon'},
    		          {id: 7, title: 'Evening'},
    		          
    		      ]
    	});
	});
	$("#removePresAppointment").click(function(){
		var $table=document.getElementById("addPrescriptionAppointment");
		var rowCount=$table.rows.length-1;
		$table.deleteRow(rowCount);
		medName.splice(rowCount,1);
		presTime.splice(rowCount,1);
	});
	
	$("#saveAppointmentDetails").click(function(){
		var newPatientData={};
		newPatientData["appointmentDate"]=$("#appointmentNotes").val();
		newPatientData["notes"]=$("#appointmentDate").val();
		symptomObj= $symptom[0].selectize;
		newPatientData["symptoms"]=symptomObj.getValue();
		var prescriptionArray=[];
		prescriptionTable=document.getElementById("addPrescriptionAppointment");
	    rowCount = prescriptionTable.rows.length;
	    for(var r=0;r<rowCount;r++)
	      {
	        var row = prescriptionTable.rows[r];
	    	var eachMedName=medName[r];
	        var medNameObj= eachMedName[0].selectize;
	        var dosage=$(row.cells[1]).find("#dosage"+r).val();
	        var startDate=$(row.cells[2]).find("#startDate"+r).datepicker('getDate'); 
	        var endDate=$(row.cells[3]).find("#endDate"+r).datepicker('getDate'); 
	        var eachPresTime=presTime[r];
	        var presTimeObj= eachPresTime[0].selectize.getValue();
	        var presTimeValue=null;
	        if(presTimeObj.length==3)
	        	presTimeValue=1;
	        else if(presTimeObj.length==2)
	        	{
	        	 if((presTimeObj[0]==5 && presTimeObj[1]==6)||(presTimeObj[0]==6 && presTimeObj[1]==5))
	        		 presTimeValue=2;
	        	 else if((presTimeObj[0]==5 && presTimeObj[1]==7)||(presTimeObj[0]==7 && presTimeObj[1]==5))
	        		 presTimeValue=3;
	        	 else
	        		 presTimeValue=4;
	        	}
	        else
	        	presTimeValue=presTimeObj[0];
	        var eachPrescription={};
	        eachPrescription["drugName"]=medNameObj.getValue();
	        eachPrescription["dosage"]=dosage;
	        eachPrescription["startDate"]=startDate;
	        eachPrescription["endDate"]= endDate;
	        eachPrescription["time"]= presTimeValue;
	        prescriptionArray[r]=eachPrescription;
	      }
	    newPatientData["prescription"]=prescriptionArray;
		alert(JSON.stringify(newPatientData));
	    $.ajax({
            type: 'post',
            url: 'http://173.39.242.206:8080/ExpertCare/rest/ECService/addAppointment?id='+patientId,
            //url: 'http://localhost:8080/ExpertCare/rest/ECService/addAppointment?id='+patientId,
            crossDomain:true,
            contentType: 'application/json; charset=utf-8',
            dataType:'json',
            data: JSON.stringify(newPatientData),
            success: function (res) {
				alert("success");
				saveFlag=true;
            },
            error:function()
            {
            	alert("Failed");
            }
        });
	    $("#patientsToday").bootstrapTable('refresh');
	    $("#patients").bootstrapTable('refresh');
	    $("#editPatientAppointment").bootstrapTable('refresh');
		 $("#addAppointModal").modal("hide");
		 $("#addAppointModal").removeData('bs.modal');
	});
});

