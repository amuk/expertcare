
$(document).ready(function(){
	var medicines=[],medName=[],presTime=[],locations=[];
	//Fetch the list of medicines already stored in the backend
	$.ajax({
        type: 'get',
        url: ' http://173.39.242.206:8080/ExpertCare/rest/ECService/getAllMeds',
        //url: ' http://localhost:8080/ExpertCare/rest/ECService/getAllMeds',
        crossDomain:true,
        contentType: 'application/json',
        dataType: 'json',
        success: function (res) {
			medicines=res.medicines;        
        }
    });
	var nowTemp = new Date();
	var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
	//Initialize the date picker
	$('#dob-input .input-group.date').datepicker({
		startView: 2,
		autoclose:true
	});
	$('#doc-input .input-group.date').datepicker({
		endDate:now,
		startView: 1,
		autoclose:true,
		todayHighlight:true	
	});
	$('#appt-input .input-group.date').datepicker({
		startDate:now,
		startView: 2,
		autoclose:true,
		todayHighlight:true	
	});
	
	//Initialize the location dropdown
	$.ajax({
        type: 'get',
        url: ' http://173.39.242.206:8080/ExpertCare/rest/ECService/getAllLocs',
        //url: ' http://localhost:8080/ExpertCare/rest/ECService/getAllLocs',
        crossDomain:true,
        contentType: 'application/json',
        dataType: 'json',
        success: function (res) {
			locations=res.locality;
			$("#selectAddLoc").selectize({
			    create: true,
			    placeholder:"Select Or Add Location",
			    valueField: 'localityId',
			    options:locations,
			    labelField: 'localityName',
				searchField: 'localityName',
				sortField: {
			        field: 'localityName',
			        direction: 'asc',
			    }
			});
        }
	});
	$("#selectAddLoc").next().find("input").attr("name","selectAddLocValue");
	
	//Initial the blood group dropdown
	var $bloodGroup=$("#bloodGroup").selectize({
	    create: false,
	    placeholder:"Select Blood Group",
	    valueField: 'id',
	    labelField: 'title',
		searchField: 'title',
        options: [
		          {id:'A+', title: 'A+'},
		          {id:'A-', title: 'A-'},
		          {id:'B+', title: 'B+'},
		          {id:'B-', title: 'B-'},
		          {id:'O+', title: 'O+'},
		          {id:'O-', title: 'O-'},
		          {id:'AB+', title: 'AB+'},
		          {id:'AB-', title: 'AB-'}
		      ],
	});
	$("#bloodGroup").next().find("input").attr("name","bloodGroupValue");
	
	//Initialize the symptoms dropdown
	$("#selectSym").selectize({
	    delimiter: ',',
	    persist: false,
	    placeholder:"Select or Add Symptoms",
	    valueField:'symptomId',
	    labelField: 'symptomName',
		searchField: 'symptomName',
		//options:symptoms,
		sortField: {
	        field: 'symptomName',
	        direction: 'asc',
	    },
	    create: function(input) {
	        return {
	        	symptomName: input,
	        	symptomId: input,
	        };}
	});
	$.ajax({
        type: 'get',
        url: ' http://173.39.242.206:8080/ExpertCare/rest/ECService/getAllSymps',
        //url: 'http://localhost:8080/ExpertCare/rest/ECService/getAllSymps',
        crossDomain:true,
        contentType: 'application/json',
        dataType: 'json',
        success: function (res) {
			symptoms=res.symptoms;
			var selectize = $("#selectSym")[0].selectize;
			selectize.clear();
			selectize.clearOptions();
			selectize.load(function(callback) {
			    callback(symptoms);
			});
        }
	});
	$("#selectSym").next().find("input").attr("name","symptomValue");
	
	//add a new prescription row
	$("#addPres").click(function () {
		var $table=document.getElementById("addPrescription");
		var rowCount=$table.rows.length;
		 $('#addPrescription').append('<tr id="pres'+(rowCount)+'"></tr>');
		 $('#pres'+rowCount).html("<td style='min-width:100px;' class='col-md-2 col-xs-2'><select name='medicine' id='medName"+rowCount+"'></select></td>" +
	    		"<td style='min-width:100px'><input  id='dosage"+rowCount+"' type='text' placeholder='Dosage' name='dosage' class='form-control notReq input-md col-xs-2 col-md-2'></td>" +
	    	    "<td style='min-width:100px' class='col-md-3 col-xs-3'><div class='input-group date'><input name='stDate' id='startDate"+rowCount+"' type='text' placeholder='Start Date'  class='form-control'></div></td>" +
	    	    "<td style='min-width:100px'  class='col-md-3 col-xs-3'><div class='input-group date'><input name='endDate' id='endDate"+rowCount+"' type='text' placeholder='End Date'  class='form-control'></div></td>" +
		    	"<td style='min-width:100px' class='col-md-2 col-xs-2'><select name='time' id='time"+rowCount+"'></select></td>");   
	 //Initialize start date date picker for each prescription
	   $("#startDate"+rowCount).datepicker({
			autoclose:true,
			todayHighlight:true	
		});
	   //Initialize end date date picker for each prescription
       $("#endDate"+rowCount).datepicker({
			autoclose:true,
			todayHighlight:true	
		});
       //Reload the medicines options for all the rows of the prescription if a new medicine is added in any of the medicine dropdown
       var reloadAllOptions=function(rowCount,input)
       {
    	   var eachMedicine={};
    	   eachMedicine["DrugId"]=input;
    	   eachMedicine["DrugName"]=input;
    	   medicines.push(eachMedicine);
    	   for(var r=0;r<medName.length;r++)
 	      {
    		//Update all medicines dropdown with the new medicine
    		if(r!=rowCount)
    		{
	 	    	var eachMedName=medName[r];
	    	    var medNameObj= eachMedName[0].selectize;
	    	    medNameObj.addOption(eachMedicine);
    		}
 	      }
       };
     //Initialize each row's medicine dropdown
        medName[rowCount]=$("#medName"+rowCount).selectize({
    		valueField: 'DrugId',
    	    labelField: 'DrugName',
    		searchField: 'DrugName',
    		create:function(input)
    		{
    			reloadAllOptions(rowCount,input);
    			 return {
    				    DrugId: input,
    	                DrugName :input
    	            };
    		},
    		placeholder:"Select/Add Medicine",
    		options: medicines,
    		sortField: {
    	        field: 'DrugName',
    	        direction: 'asc'
    	    }
    	});
       
      //Initialize each row's prescription dropdown
       presTime[rowCount]=$("#time"+rowCount).selectize({
    		create: true,
    		valueField: 'id',
    		maxItems: 3,
    	    labelField: 'title',
    		searchField: 'title',
    		placeholder:"Select Time",
    		options: [
    		          {id: 5, title: 'Morning'},
    		          {id: 6, title: 'Afternoon'},
    		          {id: 7, title: 'Evening'},
    		          
    		      ]
    	});
	});
	
	//Remove the last prescription row entered
	$("#removePres").click(function(){
		var $table=document.getElementById("addPrescription");
		var rowCount=$table.rows.length-1;
		$table.deleteRow(rowCount);
		medName.splice(rowCount,1);
		presTime.splice(rowCount,1);
	});
	
	//Save patient details 
	$("#savePatientDetails").click(function(){
		var presTableFlag=true;
		var prescriptionArray=[];
		prescriptionTable=document.getElementById("addPrescription");
	    rowCount = prescriptionTable.rows.length;
		for(var r=0;r<rowCount;r++)
			{
			  var row = prescriptionTable.rows[r];
			  $(row.cells[0]).find("div").find("div").find("input").attr("name","medicineValue");
			  $(row.cells[4]).find("div").find("div").find("input").attr("name","timeValue");
			  var eachMedName=medName[r];
		      var medNameValue= eachMedName[0].selectize.getValue();
		      var startDate=$(row.cells[2]).find("#startDate"+r).datepicker('getUTCDate'); 
		      var endDate=$(row.cells[3]).find("#endDate"+r).datepicker('getUTCDate'); 
		      var eachPresTime=presTime[r];
		      var presTimeObj= eachPresTime[0].selectize.getValue();
		      var dosage=$(row.cells[1]).find("#dosage"+r).val();
		      if(medNameValue==''|| presTimeObj.length==0|| startDate=="Invalid Date" || endDate=="Invalid Date"||dosage=='')
		    	  {
		    	  presTableFlag=false;
		    	  $("#presAlert").show();
		    	  prescriptionArray=[];
		    	  }
		      else
		    	  {
		    	  var presTimeValue=null;
			        if(presTimeObj.length==3)
			        	presTimeValue=1;
			        else if(presTimeObj.length==2)
			        	{
			        	 if((presTimeObj[0]==5 && presTimeObj[1]==6)||(presTimeObj[0]==6 && presTimeObj[1]==5))
			        		 presTimeValue=2;
			        	 else if((presTimeObj[0]==5 && presTimeObj[1]==7)||(presTimeObj[0]==7 && presTimeObj[1]==5))
			        		 presTimeValue=3;
			        	 else
			        		 presTimeValue=4;
			        	}
			        else
			        	presTimeValue=presTimeObj[0];
			        var eachPrescription={};
			        eachPrescription["drugName"]=medNameValue;
			        eachPrescription["dosage"]=dosage;
			        eachPrescription["startDate"]=startDate;
			        eachPrescription["endDate"]= endDate;
			        eachPrescription["time"]= presTimeValue;
			        prescriptionArray[r]=eachPrescription;
		    	  }
			}
		if(presTableFlag)
			{
			 $("#presAlert").hide();
			}
		$( "#addPatient" ).validate({
			ignore: ".notReq",
			debug: true,
		    success: "valid",
			rules: {
		     "inputFName": {
		    	    required: true,
		    	    accept: "[a-zA-Z]+" 
		    	  },
		     "inputLName": {
		    	    required: true,
		    	    accept: "[a-zA-Z]+" 
		    	  },
		     "addressValue":"required", 
		     "contactNo": {
		    	    required: true,
		    	    digits:true,
		    	    minlength:10,
				    maxlength:10
		    	  },
		     "alternateNo": {
			      required: true,
			      digits:true,
			      minlength:10,
			      maxlength:10
			    },
			 "dob-input":"required",
			 "doc-input":"required",
			 "appt-input":"required",
			 "selectAddLoc":"required",
			 "bloodGroup":"required",
			 "selectSym":"required"
		},
		messages: {
			 "inputFName": {
			      required: "Please enter First Name",
			      accept: "Please enter only alphabets"
			    },
			 "inputLName": {
				  required: "Please enter Last Name",
				  accept: "Please enter only alphabets"
				    },
		    "addressValue": {
			      required: "Please enter Address"
					},
		    "contactNo": {
			     required:"Please enter Contact Number",
			 	 digits:"Please enter only digits",
			     minlength:jQuery.format("{0} digits required!"),
			     maxlength:jQuery.format("{0} digits required!")
			    },
		   "alternateNo": {
				 required:"Please enter Contact Number",
				 digits:"Please enter only digits",
			     minlength:jQuery.format("{0} digits required!"),
			     maxlength:jQuery.format("{0} digits required!")
			    },
		    "dob-input":{
				 required:"Required"
			  },
			"doc-input":{
	    		 required:"Required"
				  },
            "appt-input":{
	    		 required:"Required"
                },
	 	    "selectAddLoc":{
	 	   		 required:"Required"
	 	   		},
	 	   	 "bloodGroup":{
	 	   		 required:"Required"
	 	   		},
	 	   	 "selectSym":{
	 	   		 required:"Required"
	 	   		}
		  }
	   });
		if($("#addPatient").valid() && presTableFlag)
		{
	    $("#presAlert").hide();
		//Form a JSON for the patient data
		var newPatientData={};
		newPatientData["firstName"]=$("#inputFName").val();
		newPatientData["lastName"]=$("#inputLName").val();
		newPatientData["address"]=$("#addressValue").val();
		newPatientData["contactNo"]=$("#contactNo").val();
		newPatientData["alternateNo"]=$("#alternateNo").val();
		symptomObj= $("#selectSym")[0].selectize;
		newPatientData["symptoms"]=symptomObj.getValue();
		newPatientData["dob"]=$("#dob-input .input-group.date").datepicker('getUTCDate'); 
		newPatientData["doc"]=$("#doc-input .input-group.date").datepicker('getUTCDate');
		localityObj= $("#selectAddLoc")[0].selectize;
		newPatientData["locality"]=localityObj.getValue();
		bloodGroupObj= $bloodGroup[0].selectize;
		newPatientData["bloodGroup"]=bloodGroupObj.getValue();
		newPatientData["nextAppointment"]=$("#appt-input .input-group.date").datepicker('getUTCDate');
	    newPatientData["prescription"]=prescriptionArray;
	    $.ajax({
            type: 'post',
            //url:'http://localhost:8080/ExpertCare/rest/ECService/addAPatient',
            url: ' http://173.39.242.206:8080/ExpertCare/rest/ECService/addAPatient',
            crossDomain:true,
            contentType: 'application/json; charset=utf-8',
            dataType:'json',
            data: JSON.stringify(newPatientData),
            success: function (res) {
            	//Reload the table automatically after adding a new patient to reflect the data
			    $("#patientsToday").bootstrapTable('refresh');
			    $("#patients").bootstrapTable('refresh');
            },
            error:function()
            {
            	$("#patientsToday").bootstrapTable('refresh');
                $("#patients").bootstrapTable('refresh');
            	console.log("Failed");
            }
        });
		 $("#addModal").modal("hide");
		 $("#addModal").removeData('bs.modal');
		}
	});
});

