var noOfCalls=0;
var currentRow;
$(document).ready(function(){
	$('#reports').load('Reports.html');
	var user=sessionStorage.getItem('login_user');
	document.getElementById("ec_username").innerHTML=user+'<b class="caret"></b>';

	
	if(localStorage.getItem("userid")==null)
	{
	   window.location.assign("http://173.39.242.206:8080/ExpertCare/");
       //window.location.assign("http://localhost:8080/ExpertCare/");
	} 
	else{
	$("#editPatient").hide();
	/*jQuery('.input-group.date').datepicker({
	              autoclose:true,
	              todayHighlight:true 
	       });
	       $('#appt-date-input').val("");
	       document.getElementById("symptoms").options[0].selected = true;
	       document.getElementById("locality").options[0].selected = true;*/
	 
	       //Initialize table
	       $("#patientsToday").bootstrapTable({
	              onClickRow : function(row)
	              	              {
	            	  if(row!=null)
	            		  {
	                     
	                     currentRow=row.PatientId.$oid;
	                     //alert(currentRow);
	                     $("#todaysAppointments").hide();
	                     $("#editPatient").show();
	                     $("#saveedit").hide();
	                     $("#canceledit").hide();
	                     $.ajax({
	                         type: 'get',
	                         url: ' http://173.39.242.206:8080/ExpertCare/rest/ECService/getAPatient?id='+row.PatientId.$oid,
	                         //url: ' http://localhost:8080/ExpertCare/rest/ECService/getAPatient?id='+row.PatientId.$oid,
	                         crossDomain:true,
	                         contentType: 'application/json',
	                         dataType: 'json',
	                         success: function (res) {
	                        	 $("#editpid").val(res.PatientId.$oid); 
	                        	 sessionStorage.setItem('PatientId', res.PatientId.$oid);

	                        	 $("#editFName").val(res.firstName); 

	                        	 $("#editLName").val(res.lastName); 

	                        	 $("#editAddress").val(res.address); 

	                        	 $("#editContact").val(res.contactNo); 

	                        	 $("#editAlternate").val(res.alternateNo); 

	                        	 $("#editLocality").val(res.locality); 

	                        	 $("#editBloodgrp").val(res.bloodGroup); 

	                        	 $("#editdoc").val(res.doc); 

	                        	 $("#editdob").val(res.dob); 
	                         },
	                        error:function()
	                        {
	                        	console.log("Failure");
	                        }
	                     });
	                     
	                     $("#editPatientAppointments").bootstrapTable({
	       	              method: 'get',
	       	              url: 'http://173.39.242.206:8080/ExpertCare/rest/ECService/getAllAppmnts?id='+row.PatientId.$oid,
	       	              //url: 'http://localhost:8080/ExpertCare/rest/ECService/getAllAppmnts?id='+row.PatientId.$oid, //service URL later
	       	              cache: false,
	       	              striped: true,
	       	              pagination: true,
	       	              pageSize: 2,
	       	              pageList: [10, 25, 50, 100, 200],
	       	              search: true,
	       	              showColumns: true,
	       	              showRefresh: true,
	       	              minimumCountColumns: 2,
	       	              clickToSelect: true,
	       	              responseHandler : tableResHandlerAppointments,
	       	              columns: [{
	       	                     field: 'PatientId',
	       	                     title: 'Patient Id',
	       	                     visible:false
	       	      
	       	              },
	       	              {
	       	                     field: 'appointmentDate',
	       	                     title: 'Appointment Date:',
	       	      
	       	              },
	       	              {
	       	                     field: 'symptoms',
	       	                     title: 'Symptoms:',
	       	 
	       	              },
	       	              {
	       	                     field: 'prescription',
	       	                     title: 'Prescription:',
	       	                     formatter: appointmentFormatter,
	       	 
	       	              },
	       	              {
	       	                     field: 'notes',
	       	                     title: 'Notes:',
	       	 
	       	              }
	       	              ]
	       	       });
	                     $("#patientNotifications").bootstrapTable({
	          	           method: 'get',
	          	           url: 'ec_sample-notif.json?id='+row.PatientId.$oid, //service URL later
	          	           cache: false,
	          	           striped: true,
	          	           pagination: true,
	          	           pageSize: 2,
	          	           pageList: [10, 25, 50, 100, 200],
	          	           search: true,
	          	           showColumns: true,
	          	           showRefresh: true,
	          	           minimumCountColumns: 2,
	          	           clickToSelect: true,
	          	           responseHandler : tableResHandlerNotifications,
	          	           columns: [{
	          	                  field: 'firstName',
	          	                  title: 'First Name:',
	          	   
	          	           },
	          	         {
	          	                  field: 'lastName',
	          	                  title: 'Last Name:',
	          	   
	          	           },
	          	           {
	          	                  field: 'time',
	          	                  title: 'Time:',
	          	
	          	           },
	          	           
	          	           {
	          	                  field: 'response',
	          	                  title: 'Response:',
	          	
	          	           }
	          	           ]
	          	    });
	       	       
	                     /*$.ajax({
	                   type: 'get',
	                   url: 'ec_patient_details.json',
	                   contentType: 'application/json',
	                   dataType: 'json',
	                   success: function (res) {
	                                  var result = res.patientDetails;
	                                  alert(result);
	                                  $("#todaysAppointments").hide();
	                                  $("#editPatient").show();
	                   }
	               });*/
	            		  }},
	              method: 'get',
	              url: 'http://173.39.242.206:8080/ExpertCare/rest/ECService/getTodaysAppointments',
	              //url: 'http://localhost:8080/ExpertCare/rest/ECService/getTodaysAppointments', //service URL later
	              cache: false,
	              striped: true,
	              pagination: true,
	              pageSize: 50,
	              pageList: [10, 25, 50, 100, 200],
	              search: true,
	              showColumns: true,
	              showRefresh: true,
	              minimumCountColumns: 2,
	              clickToSelect: true,
	              responseHandler : tableResHandler,
	              columns: [{
	                     field: 'PatientId',
	                     title: 'Patient Id',
	                    visible:false
	              },
	              {
	                     field: 'firstName',
	                     title: 'Patient Name',
	                     align: 'center',
	                     valign: 'middle',
	                     sortable: true,
	                     formatter : nameFormatter
	              },
	              {
	                     field: 'noOfMonths',
	                     title: 'No of months',
	                     align: 'center',
	                     valign: 'middle',
	                     sortable: true
	              },
	            /*  {
	                     field: 'lastVisitDate',
	                     title: 'Last Visit Date',
	                     align: 'center',
	                     valign: 'middle',
	                     sortable: true
	              },*/
	              {
	                     field: 'symptoms',
	                     title: 'Symptoms',
	                     align: 'center',
	                     valign: 'middle',
	                     sortable: true
	              }, {
	                     field: 'locality',
	                     title: 'Locality',
	                     align: 'center',
	                     valign: 'middle',
	                     visible: false
	              }]
	       });
	       //Initialize table
	       $("#patients").bootstrapTable({
	              onClickRow : function(row)
	              {     
	                     //alert(row._id.$oid);
	              },
	              method: 'get',	              
	              url: 'http://173.39.242.206:8080/ExpertCare/rest/ECService/getAllPatients',
	              //url: 'http://localhost:8080/ExpertCare/rest/ECService/getAllPatients', //service URL later
	              cache: false,
	              striped: true,
	              pagination: true,
	              pageSize: 50,
	              pageList: [10, 25, 50, 100, 200],
	              search: true,
	              showColumns: true,
	              showRefresh: true,
	              minimumCountColumns: 2,
	              clickToSelect: true,
	              responseHandler : tableResHandler,
	              columns: [{
	                     field: 'firstName',
	                     title: 'Patient Name',
	                     align: 'center',
	                     valign: 'middle',
	                     sortable: true,
	                     formatter : nameFormatter
	              },
	              {
	                     field: 'age',
	                     title: 'Age',
	                     align: 'center',
	                     valign: 'middle',
	                     sortable: true
	              },
	              {
	                     field: 'noOfMonths',
	                     title: 'No of months',
	                     align: 'center',
	                     valign: 'middle',
	                     sortable: true
	              },
	              /*{
	                     field: 'lastVisitDate',
	                     title: 'Last Visit Date',
	                     align: 'center',
	                     valign: 'middle',
	                     sortable: true
	              },*/ {
	                     field: 'nextAppointment',
	                     title: 'Next Appointment Date',
	                     align: 'center',
	                     valign: 'middle',
	                     sortable: true
	              }, {
	                     field: 'symptoms',
	                     title: 'Symptoms',
	                     align: 'center',
	                     valign: 'middle',
	                     sortable: true
	              }, {
	                     field: 'locality',
	                     title: 'Locality',
	                     align: 'center',
	                     valign: 'middle',
	                     visible: false
	              }]
	       });
	      
	       //initialize table
	      
	      
	      
	       function nameFormatter(value,row)
	       {
	              var data=value+' '+row.lastName;
	              return data;
	             
	       }
	             
	       /*$("#submit").click(function () {
	              var symptom = document.getElementById("symptoms").value;
	              var locality = document.getElementById("locality").value;
	              var appt = $("#appt-date-input").val();
	              alert("Values selected ->"+symptom+" "+locality+" "+appt);
	              $.ajax({
	            type: 'get',
	            url: 'ec_sample-1.json',
	            contentType: 'application/json',
	            dataType: 'json',
	            success: function (res) {
	                           var result = res.patients;
	                $("#patients").bootstrapTable('load', result);
	            }
	        });
	       });*/
	}
});
 
function backToAppointments()
{
       $("#editPatient").hide();
       $("#todaysAppointments").show();
       }
 
function tableResHandler(res)
{
       return res.patients;
}
 
function tableResHandlerAppointments(res)
{
       return res.appointmentInfo;
}

function tableResHandlerNotifications(res)
{
       return res.notifications;
}
 
function appointmentFormatter(value,row)
{
noOfCalls++;
var prescriptionTable='<div class="table-responsive"> <table class="table"><tr><th style="min-width:100px" class="col-md-3 col-xs-3">Drug Name</th><th style="min-width:100px" class="col-md-3 col-xs-3">Dosage</th><th style="min-width:100px" class="col-md-3 col-xs-3">Start Date</th><th style="min-width:100px" class="col-md-3 col-xs-3">End date</th><th style="min-width:100px" class="col-md-3 col-xs-3">Time</th></tr>';
for(var i=0;i<row.prescription.length;i++)
{
var drugName=row.prescription[i].drugName;
var dosage=row.prescription[i].dosage;
var startDate=row.prescription[i].startDate;
var endDate=row.prescription[i].endDate;
var time=row.prescription[i].time;
 
prescriptionTable+='<tr><td style="min-width:100px" class="col-md-3 col-xs-3">'+drugName+'</td><td style="min-width:100px" class="col-md-3 col-xs-3">'+dosage+'</td><td style="min-width:100px" class="col-md-3 col-xs-3">'+startDate+'</td><td style="min-width:100px" class="col-md-3 col-xs-3">'+endDate+'</td><td style="min-width:100px" class="col-md-3 col-xs-3">'+time+'</td></tr>';
}
prescriptionTable+='</table></div>';
 
return prescriptionTable;
}
 
$("#editPatientButton").click(function(){
	//document.getElementById('editpid').disabled = false;
	document.getElementById('editFName').disabled = false;
	document.getElementById('editLName').disabled = false;
	document.getElementById('editAddress').disabled = false;
	document.getElementById('editContact').disabled = false;
	document.getElementById('editAlternate').disabled = false;
	document.getElementById('editLocality').disabled = false;
	document.getElementById('editBloodgrp').disabled = false;
	document.getElementById('editdob').disabled = false;
	document.getElementById('editdoc').disabled = false;
	document.getElementById("saveedit").style.display = "block";
	document.getElementById("canceledit").style.display = "block";
    $("#saveedit").show();
    $("#canceledit").show();

	});
$("#canceledit").click(function(){
	//document.getElementById('editpid').disabled = true;
	document.getElementById('editFName').disabled = true;
	document.getElementById('editLName').disabled = true;
	document.getElementById('editAddress').disabled = true;
	document.getElementById('editContact').disabled = true;
	document.getElementById('editAlternate').disabled =true;
	document.getElementById('editLocality').disabled =true;
	document.getElementById('editBloodgrp').disabled = true;
	document.getElementById('editdob').disabled = true;
	document.getElementById('editdoc').disabled = true;
    $("#saveedit").hide();
    $("#canceledit").hide();
    backToAppointments();
	});
$("#saveedit").click(function(){
	var updatedPatientData={};
	updatedPatientData["PatientId"]=$("#editpid").val();
	updatedPatientData["firstName"]=$("#editFName").val();
	updatedPatientData["lastName"]=$("#editLName").val();
	updatedPatientData["address"]=$("#editAddress").val();
	updatedPatientData["contactNo"]=$("#editContact").val();
	updatedPatientData["alternateNo"]=$("#editAlternate").val();
	updatedPatientData["locality"]=$("#editLocality").val();
	updatedPatientData["bloodGroup"]=$("#editBloodgrp").val();
	updatedPatientData["dob"]=$("#editdob").val();  
	updatedPatientData["doc"]=$("#editdoc").val();
    $("#saveedit").hide();
    $("#canceledit").hide();
    $.ajax({
        type: 'post',
        //url:'http://localhost:8080/ExpertCare/rest/ECService/updateAPatient',
        url: ' http://173.39.242.206:8080/ExpertCare/rest/ECService/updateAPatient',
        crossDomain:true,
        contentType: 'application/json; charset=utf-8',
        dataType:'json',
        data: JSON.stringify(updatedPatientData),
        success: function (res) {
        	//alert("success")
        	//Reload the table automatically after adding a new patient to reflect the data
		    $("#patientsToday").bootstrapTable('refresh');
		    $("#patients").bootstrapTable('refresh');
		    backToAppointments();
        },
        error:function()
        {
        	 $("#patientsToday").bootstrapTable('refresh');
		 $("#patients").bootstrapTable('refresh');
        	console.log("Failed");
        }
    });

	});


