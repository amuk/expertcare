$(document).ready(function(){	
	
	//Initialize notifications table
	$("#notifications").bootstrapTable({
		cache: false,
		striped: false,
		pagination: false,
		showColumns: false,
		minimumCountColumns: 2,
		clickToSelect: false,
		columns: [{
			field: 'firstName',
			title: 'First Name'
		},{
			field: 'lastName',
			title: 'Last Name'
		},{
			field: 'time',
			title: 'Time of the day'
		},{
			field: 'date',
			title: 'Date',			
		},{
			field: 'response',
			title: 'Response'
		}]
	});	
	
	//Pull notifications for the first time
	$.ajax({
	    type: 'get',
	    url: 'http://173.39.242.206:8080/ExpertCare/rest/ECService/getNotifs',
	    //url: 'http://localhost:8080/ExpertCare/rest/ECService/getNotifs',
	    contentType: 'application/json',
	    dataType: 'json',
	    success: function (res) {
			var result = res.notifications;
	        $("#notifications").bootstrapTable('load', result);
	        var notif = document.getElementById("notifCount");
	        notif.setAttribute("data-notifications", res.notifCount);
	    }
	})
	
	var lastNotification = Date.now();
    
    //Pull new notifications every 2 minutes showing the count in the UI
    var timer, delay = 7200; 
    timer = setInterval(pullNotifications, delay);
    
    function pullNotifications(){
		$.ajax({
            type: 'get',
            url: 'http://173.39.242.206:8080/ExpertCare/rest/ECService/getNotifs?lastNotif='+lastNotification,
            //url: 'http://localhost:8080/ExpertCare/rest/ECService/getNotifs?lastNotif='+lastNotification,
            contentType: 'application/json',
            dataType: 'json',
            success: function (res) {
				var result = res.notifications;
                $("#notifications").bootstrapTable('load', result);
                var notif = document.getElementById("notifCount");
                notif.setAttribute("data-notifications", res.notifCount);
            }
        })
        lastNotification = Date.now();
    }
});