
 
$(document).ready(function(){
	$('.input-daterange').datepicker({
	    startView: 2,
	    autoclose: true,
	    todayHighlight: true
	});
	$("#submit").click(function () {
	var paramSearch={};
	var startDate=$("#startDateR").datepicker('getUTCDate');
	paramSearch["startDate"]=startDate;
	var endDate=$("#endDateR").datepicker('getUTCDate');
	paramSearch["endDate"]=endDate;
    $.ajax({
     type: 'post',
     url:'http://173.39.242.206:8080/ExpertCare/rest/ECService/doAnalytics',
     //url:'http://localhost:8080/ExpertCare/rest/ECService/doAnalytics',
     dataType:"json",
    data: JSON.stringify(paramSearch),
    contentType: 'application/json; charset=utf-8',
    success: function (res) {
    	//alert(JSON.stringify(res));
    	 if(res!=null)
    		{
		    	 maxMedName=res.maxName;
		    	 maxMedCount=res.maxCount;
		         medicationAlert="Medication "+maxMedName+" was suggested "+maxMedCount+" times between "+startDate+" to "+endDate;
		         $("#reportCountHeader").html(medicationAlert);
		         var data = new google.visualization.DataTable();
		         data.addColumn('string', 'Medicine');
		         data.addColumn('number', 'Count');
		         for (var key in res) {
		        	 if (res.hasOwnProperty(key) && key!="maxName" && key!="maxCount") {
		        	  data.addRow([key,parseInt(res[key])]);
		        	  }
		        	}
		         // Set chart options
		         var options = {'title':'Drug Wise Frequency',
		                        'width':700,
		                        'height':800,
		                       is3D: true};

		         // Instantiate and draw our chart, passing in some options.
		         var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
		         chart.draw(data, options);
		    	}
		       else
		    	   {
		    	   $("#reportCountHeader").html("No Medication was sugggested during this date range");
		    	   }
    },
    error:function()
    {
    	 $("#chart_div").html("");
    	 $("#reportCountHeader").html("Please enter a date range to search");
    	console.log("Failed");
    }
    });
});
});