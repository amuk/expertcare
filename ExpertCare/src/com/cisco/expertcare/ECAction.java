package com.cisco.expertcare;


import java.net.UnknownHostException;
import com.mongodb.DB;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;

public class ECAction {
	
	public DB initialize() {

		final MongoClient mongoClient;
		final DB ECDatabase;

		try {
			mongoClient = new MongoClient(new MongoClientURI(
					"mongodb://localhost"));
			ECDatabase = mongoClient.getDB("expertcare");	
			
			System.out.println("Initialized");
			return ECDatabase;
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}