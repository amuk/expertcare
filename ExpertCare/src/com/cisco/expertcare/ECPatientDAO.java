package com.cisco.expertcare;

import scheduler.CallNow.*;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.bson.types.ObjectId;
import org.joda.time.LocalDate;
import org.joda.time.Period;
import org.joda.time.PeriodType;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.*;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.util.JSON;

public class ECPatientDAO {

	DBCollection patientdataCollection;
	DBCollection medicationCollection;
	DBCollection symptomCollection;
	DBCollection localityCollection;
	DBCollection lastVisitDatesCollection;
	DBCollection ivrresponsesCollection;	
	DBCollection appmedmapCollection;

	public ECPatientDAO(final DB ECDatabase) {
		patientdataCollection = ECDatabase.getCollection("newpatientdata");
		medicationCollection = ECDatabase.getCollection("medication");
		symptomCollection = ECDatabase.getCollection("symptoms");
		localityCollection = ECDatabase.getCollection("localities");
		lastVisitDatesCollection = ECDatabase
				.getCollection("lastVisitDatesCollection");
		ivrresponsesCollection = ECDatabase.getCollection("IVRresponse");
		appmedmapCollection = ECDatabase.getCollection("drugdetails");
	}

	/*
	 * Add details about a patient to the database
	 * ====================================================================
	 */
	public String addPatientDetails(String jsonNewPatient) {
		//System.out.println("Patient String: " + jsonNewPatient);
		Date dob = null;
		Date doc = null;
		Date nextAppointment = null;
		String sdob, sdoc, snextAppointment, contactNo;
				
		try {
			BasicDBObject document = (BasicDBObject) JSON.parse(jsonNewPatient);

			sdob = (String) document.getString("dob");
			sdoc = (String) document.get("doc");
			snextAppointment = (String) document.get("nextAppointment");

			if (sdob != null)
				dob = StringToDate(sdob);
			if (sdoc != null)
				doc = StringToDate(sdoc);
			if (snextAppointment != null)
				nextAppointment = StringToDate(snextAppointment);
			
			contactNo = "91" + document.getString("contactNo");
			
			document.remove("dob");
			document.remove("doc");
			document.remove("nextAppointment");
			document.remove("contactNo");

			document.put("dob", dob);
			document.put("doc", doc);
			document.put("nextAppointment", nextAppointment);
			document.put("contactNo", contactNo);

			List<BasicDBObject> newPrescription = editPrescription(jsonNewPatient);
			document.remove("prescription");
			document.put("prescription", newPrescription);

			ObjectId id = new ObjectId();
			document.put("PatientId", id);
			patientdataCollection.insert(document);
			String patId = id.toString();

			String newPatId = "{\"PatientId\" : \"" + patId + "\"}";
			
			// Add the medicine, locality and symtoms to the db if they do not
			// already exist
			addMedicineToDB(jsonNewPatient);			
			addLocalityToDB(jsonNewPatient);			
			addSymptomsToDB(jsonNewPatient);

			// Insert today as last visit date in lastVisitDatesCollection
			updateLastVisitInfo(document.toString(), patId);
			
			JSONObject jSONObject = new JSONObject(jsonNewPatient);
			JSONArray presArr = jSONObject.getJSONArray("prescription");
			for (int i = 0; i < presArr.length(); i++) {
				JSONObject anotherjsonObject = presArr.getJSONObject(i);
				String drugName = anotherjsonObject.getString("drugName");
				
				BasicDBObject docformap = new BasicDBObject();
				docformap.put("_id", new ObjectId());
				docformap.put("date", new Date());
				docformap.put("drugName", drugName);
				
				appmedmapCollection.insert(docformap);
			}
			
			SingleCall trigger = new SingleCall();
			trigger.schedule(patId, document.getString("firstName"),
					document.getString("lastName"),
					document.getString("contactNo"),
					getAllStartDates(jsonNewPatient),
					getAllEndDates(jsonNewPatient));
			
			ResponseChecker checkResponse = new ResponseChecker();
			checkResponse.schedule(patId, document.getString("firstName"),
					document.getString("lastName"),
					document.getString("contactNo"),
					getAllStartDates(jsonNewPatient),
					getAllEndDates(jsonNewPatient));

			return newPatId;
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	public List<String> getAllStartDates(String jsonNewPatient) {
		String formattedDate;
		List<String> startDates = new ArrayList<String>();
		try {
			JSONObject jSONObject = new JSONObject(jsonNewPatient);
			JSONArray presArr = jSONObject.getJSONArray("prescription");
			for (int i = 0; i < presArr.length(); i++) {
				JSONObject anotherjsonObject = presArr.getJSONObject(i);
				int time = anotherjsonObject.getInt("time");
				String sstartDate = anotherjsonObject.getString("startDate");				
				Date date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
						.parse(sstartDate);
				Calendar cal = Calendar.getInstance();
				cal.setTime(date);
				if(time == 1){
					cal.set(Calendar.HOUR_OF_DAY, 7);	
					cal.set(Calendar.MINUTE, 0);
					formattedDate = new SimpleDateFormat(
							"dd-MM-yyyy HH:mm:ss").format(cal.getTime());
					startDates.add(formattedDate);
					
					cal.set(Calendar.HOUR_OF_DAY, 12);	
					cal.set(Calendar.MINUTE, 0);
					formattedDate = new SimpleDateFormat(
							"dd-MM-yyyy HH:mm:ss").format(cal.getTime());
					startDates.add(formattedDate);
					
					cal.set(Calendar.HOUR_OF_DAY, 17);
					cal.set(Calendar.MINUTE, 0);
					formattedDate = new SimpleDateFormat(
							"dd-MM-yyyy HH:mm:ss").format(cal.getTime());
					startDates.add(formattedDate);
				}
				else if(time == 2){
					cal.set(Calendar.HOUR_OF_DAY, 7);
					cal.set(Calendar.MINUTE, 0);
					formattedDate = new SimpleDateFormat(
							"dd-MM-yyyy HH:mm:ss").format(cal.getTime());
					startDates.add(formattedDate);
					
					cal.set(Calendar.HOUR_OF_DAY, 12);		
					cal.set(Calendar.MINUTE, 0);
					formattedDate = new SimpleDateFormat(
							"dd-MM-yyyy HH:mm:ss").format(cal.getTime());
					startDates.add(formattedDate);
				}
				else if(time == 3){
					cal.set(Calendar.HOUR_OF_DAY, 7);
					cal.set(Calendar.MINUTE, 0);
					formattedDate = new SimpleDateFormat(
							"dd-MM-yyyy HH:mm:ss").format(cal.getTime());
					startDates.add(formattedDate);
					
					cal.set(Calendar.HOUR_OF_DAY, 17);	
					cal.set(Calendar.MINUTE, 0);
					formattedDate = new SimpleDateFormat(
							"dd-MM-yyyy HH:mm:ss").format(cal.getTime());
					startDates.add(formattedDate);
				}
				else if(time == 4){
					cal.set(Calendar.HOUR_OF_DAY, 12);	
					cal.set(Calendar.MINUTE, 0);
					formattedDate = new SimpleDateFormat(
							"dd-MM-yyyy HH:mm:ss").format(cal.getTime());
					startDates.add(formattedDate);
					
					cal.set(Calendar.HOUR_OF_DAY, 17);	
					cal.set(Calendar.MINUTE, 0);
					formattedDate = new SimpleDateFormat(
							"dd-MM-yyyy HH:mm:ss").format(cal.getTime());
					startDates.add(formattedDate);
				}
				else if(time == 5){
					cal.set(Calendar.HOUR_OF_DAY, 7);	
					cal.set(Calendar.MINUTE, 0);
					formattedDate = new SimpleDateFormat(
							"dd-MM-yyyy HH:mm:ss").format(cal.getTime());
					startDates.add(formattedDate);
				}
				else if(time == 6){
					cal.set(Calendar.HOUR_OF_DAY, 12);		
					cal.set(Calendar.MINUTE, 0);
					formattedDate = new SimpleDateFormat(
							"dd-MM-yyyy HH:mm:ss").format(cal.getTime());
					startDates.add(formattedDate);
				}
				else{
					cal.set(Calendar.HOUR_OF_DAY, 17);	
					cal.set(Calendar.MINUTE, 0);
					formattedDate = new SimpleDateFormat(
							"dd-MM-yyyy HH:mm:ss").format(cal.getTime());
					startDates.add(formattedDate);
				}
			}
			return startDates;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	public List<String> getAllEndDates(String jsonNewPatient) {
		List<String> endDates = new ArrayList<String>();
		try {
			JSONObject jSONObject = new JSONObject(jsonNewPatient);
			JSONArray presArr = jSONObject.getJSONArray("prescription");
			for (int i = 0; i < presArr.length(); i++) {
				JSONObject anotherjsonObject = presArr.getJSONObject(i);
				int time = anotherjsonObject.getInt("time");
				String sendDate = anotherjsonObject.getString("endDate");
				Date date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
						.parse(sendDate);
				String formattedDate = new SimpleDateFormat(
						"dd-MM-yyyy HH:mm:ss").format(date);
				Calendar cal = Calendar.getInstance();
				cal.setTime(date);
				if(time == 1){
					cal.set(Calendar.HOUR_OF_DAY, 7);		
					cal.set(Calendar.MINUTE, 0);
					formattedDate = new SimpleDateFormat(
							"dd-MM-yyyy HH:mm:ss").format(cal.getTime());
					endDates.add(formattedDate);
					
					cal.set(Calendar.HOUR_OF_DAY, 12);	
					cal.set(Calendar.MINUTE, 0);
					formattedDate = new SimpleDateFormat(
							"dd-MM-yyyy HH:mm:ss").format(cal.getTime());
					endDates.add(formattedDate);
					
					cal.set(Calendar.HOUR_OF_DAY, 17);	
					cal.set(Calendar.MINUTE, 0);
					formattedDate = new SimpleDateFormat(
							"dd-MM-yyyy HH:mm:ss").format(cal.getTime());
					endDates.add(formattedDate);
				}
				else if(time == 2){
					cal.set(Calendar.HOUR_OF_DAY, 7);		
					cal.set(Calendar.MINUTE, 0);
					formattedDate = new SimpleDateFormat(
							"dd-MM-yyyy HH:mm:ss").format(cal.getTime());
					endDates.add(formattedDate);
					
					cal.set(Calendar.HOUR_OF_DAY, 12);		
					cal.set(Calendar.MINUTE, 0);
					formattedDate = new SimpleDateFormat(
							"dd-MM-yyyy HH:mm:ss").format(cal.getTime());
					endDates.add(formattedDate);
				}
				else if(time == 3){
					cal.set(Calendar.HOUR_OF_DAY, 7);	
					cal.set(Calendar.MINUTE, 0);
					formattedDate = new SimpleDateFormat(
							"dd-MM-yyyy HH:mm:ss").format(cal.getTime());
					endDates.add(formattedDate);
					
					cal.set(Calendar.HOUR_OF_DAY, 17);	
					cal.set(Calendar.MINUTE, 0);
					formattedDate = new SimpleDateFormat(
							"dd-MM-yyyy HH:mm:ss").format(cal.getTime());
					endDates.add(formattedDate);
				}
				else if(time == 4){
					cal.set(Calendar.HOUR_OF_DAY, 12);	
					cal.set(Calendar.MINUTE, 0);
					formattedDate = new SimpleDateFormat(
							"dd-MM-yyyy HH:mm:ss").format(cal.getTime());
					endDates.add(formattedDate);
					
					cal.set(Calendar.HOUR_OF_DAY, 17);	
					cal.set(Calendar.MINUTE, 0);
					formattedDate = new SimpleDateFormat(
							"dd-MM-yyyy HH:mm:ss").format(cal.getTime());
					endDates.add(formattedDate);
				}
				else if(time == 5){
					cal.set(Calendar.HOUR_OF_DAY, 7);
					cal.set(Calendar.MINUTE, 0);
					formattedDate = new SimpleDateFormat(
							"dd-MM-yyyy HH:mm:ss").format(cal.getTime());
					endDates.add(formattedDate);
				}
				else if(time == 6){
					cal.set(Calendar.HOUR_OF_DAY, 12);	
					cal.set(Calendar.MINUTE, 0);
					formattedDate = new SimpleDateFormat(
							"dd-MM-yyyy HH:mm:ss").format(cal.getTime());
					endDates.add(formattedDate);
				}
				else{
					cal.set(Calendar.HOUR_OF_DAY, 17);	
					cal.set(Calendar.MINUTE, 0);
					formattedDate = new SimpleDateFormat(
							"dd-MM-yyyy HH:mm:ss").format(cal.getTime());
					endDates.add(formattedDate);
				}
			}
			return endDates;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	/*
	 * Edit prescription - convert strings to dates
	 * ====================================================================
	 */
	public List<BasicDBObject> editPrescription(String jsonNewPatient) {
		List<BasicDBObject> prescriptionList = new ArrayList<BasicDBObject>();		
		try {
			JSONObject jSONObject = new JSONObject(jsonNewPatient);
			JSONArray presArr = jSONObject.getJSONArray("prescription");
			for (int i = 0; i < presArr.length(); i++) {
				JSONObject anotherjsonObject = presArr.getJSONObject(i);
				String drugName = anotherjsonObject.getString("drugName");
				String dosage = anotherjsonObject.getString("dosage");
				String sstartDate = anotherjsonObject.getString("startDate");
				String sendDate = anotherjsonObject.getString("endDate");
				int time = anotherjsonObject.getInt("time");
				Date startDate = StringToDate(sstartDate);
				Date endDate = StringToDate(sendDate);				
				BasicDBObject document = new BasicDBObject();
				document.put("drugName", drugName);
				document.put("dosage", dosage);
				document.put("startDate", startDate);
				document.put("endDate", endDate);
				document.put("time", time);
				prescriptionList.add(document);
			}
			return prescriptionList;
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
	}

	/*
	 * Convert String to Date
	 * ====================================================================
	 */
	public Date StringToDate(String sdate) {
		SimpleDateFormat formatter;
		formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		//formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
		Date date;
		try {
			date = formatter.parse(sdate.substring(0, 24));
			return date;
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
	}

	/*
	 * Update lastVsitDate
	 * ====================================================================
	 */
	public void updateLastVisitInfo(String spatient, String patId) {
		SimpleDateFormat formatter = new SimpleDateFormat(
				"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

		BasicDBObject doc = (BasicDBObject) JSON.parse(spatient);
		// System.out.println(doc.getString("PatientId"));
		BasicDBObject whereQuery = new BasicDBObject("PatientId", new ObjectId(
				patId));
		DBCursor cursor = lastVisitDatesCollection.find(whereQuery);

		if (cursor.hasNext() == false) {
			DBObject dock = new BasicDBObject();
			dock.put("PatientId", new ObjectId(patId));
			List<BasicDBObject> appointmentInfo = new ArrayList<BasicDBObject>();
			appointmentInfo.add(new BasicDBObject()
					.append("appointmentDate", new Date())
					.append("symptoms", doc.get("symptoms"))
					.append("prescription", doc.get("prescription")));
			dock.put("appointmentInfo", appointmentInfo);
			lastVisitDatesCollection.insert(dock);
		} else {
			try {				
				/*String onextAppointment = doc.getString("nextAppointment");				
				Date dnextAppointment = StringToDate(onextAppointment);
				String snextAppointment = formatter.format(dnextAppointment);
				Date nextAppointment = StringToDate(snextAppointment);*/
				
				Date dnextAppointment;
				if (doc.get("nextAppointment") instanceof Date == true) {
					Object onextAppointment = doc.get("nextAppointment");
					dnextAppointment = (Date) onextAppointment;
				} else {
					String onextAppointment = (String) doc.get("nextAppointment");
					dnextAppointment = StringToDate(onextAppointment);
				}
				String snextAppointment = formatter.format(dnextAppointment);
				Date nextAppointment = StringToDate(snextAppointment);
				
				//List<BasicDBObject> newPrescription = editPrescription(spatient);
				DBObject updateObj = new BasicDBObject(
						"appointmentInfo",
						new BasicDBObject()
								.append("appointmentDate", nextAppointment)
								.append("symptoms", doc.get("symptoms"))
								.append("prescription", doc.get("prescription")));
				lastVisitDatesCollection.update(whereQuery, new BasicDBObject(
						"$push", updateObj));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/*
	 * Add new appointment
	 * ====================================================================
	 */
	public String addNewAppointment(String spatient) {
		SimpleDateFormat formatter = new SimpleDateFormat(
				"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

		BasicDBObject doc = (BasicDBObject) JSON.parse(spatient);
		BasicDBObject whereQuery = new BasicDBObject("PatientId", new ObjectId(
				doc.getString("PatientId")));
		// System.out.println(doc.getString("PatientId"));
		DBCursor cursor = patientdataCollection.find(whereQuery);

		DBObject document = cursor.next();

		Date dnextAppointment;
		if (document.get("nextAppointment") instanceof Date == true) {
			Object onextAppointment = document.get("nextAppointment");
			dnextAppointment = (Date) onextAppointment;
		} else {
			String onextAppointment = (String) document.get("nextAppointment");
			dnextAppointment = StringToDate(onextAppointment);
		}
		String snextAppointment = formatter.format(dnextAppointment);
		Date nextAppointment = StringToDate(snextAppointment);

		Date today = new Date();
		Date newAppointment = StringToDate(doc.getString("appointmentDate"));
		if (nextAppointment.before(today)) {			
			updateLastVisitInfo(document.toString(), doc.getString("PatientId"));
		}
		
		List<BasicDBObject> newPrescription = editPrescription(spatient);
		
		BasicDBObject newDocument = new BasicDBObject();
		newDocument.append(
				"$set",
				new BasicDBObject().append("nextAppointment", newAppointment)
						.append("symptoms", doc.get("symptoms"))
						.append("prescription", newPrescription)
						.append("notes", doc.getString("notes")));

		patientdataCollection.update(whereQuery, newDocument);

		String updatedPatId = "{\"PatientId\" : \""
				+ document.get("PatientId").toString() + "\"}";

		return updatedPatId;

	}

	/*
	 * Add medicine to DB if it does not already exist
	 * ====================================================================
	 */
	public void addMedicineToDB(String jsonNewPatient) {
		BasicDBObject document = new BasicDBObject();
		String medName;
		ArrayList<String> allMedsArray = new ArrayList<String>();
		allMedsArray = getAllMedicinesArray();
		try {
			JSONObject jSONObject = new JSONObject(jsonNewPatient);
			JSONArray presArr = jSONObject.getJSONArray("prescription");
			for (int i = 0; i < presArr.length(); i++) {
				JSONObject anotherjsonObject = presArr.getJSONObject(i);
				medName = anotherjsonObject.getString("drugName");
				if (allMedsArray.contains(medName) == false) {
					document.put("drugName", medName);
					document.put("drugId", medName);
					medicationCollection.insert(document);
				}
				allMedsArray = getAllMedicinesArray();
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	
	/*
	 * Add locality to DB if it does not already exist
	 * ====================================================================
	 */
	public void addLocalityToDB(String jsonNewPatient){	
		BasicDBObject doc = new BasicDBObject();
		String locality;
		ArrayList<String> allLocalityArray = new ArrayList<String>();
		allLocalityArray = getAllLocalityArray();
		try{
			BasicDBObject document = (BasicDBObject) JSON.parse(jsonNewPatient);
			locality = document.getString("locality");
			if(allLocalityArray.contains(locality) == false){
				doc.put("localityName", locality);
				doc.put("localityId", locality);	
				localityCollection.insert(doc);
			}			
		}catch(Exception e){
			e.printStackTrace();
		}		
	}
	
	
	/*
	 * Add symptoms to DB if it does not already exist
	 * ====================================================================
	 */
	public void addSymptomsToDB(String jsonNewPatient) {
		BasicDBObject doc = new BasicDBObject();
		String symptoms, symp;
		ArrayList<String> allSymptomsArray = new ArrayList<String>();
		allSymptomsArray = getAllSymptomsArray();
		try {
			BasicDBObject document = (BasicDBObject) JSON.parse(jsonNewPatient);
			symptoms = document.getString("symptoms");
			List<String> items = Arrays.asList(symptoms.split("\\s*,\\s*"));
			for (int i = 0; i < items.size(); i++) {
				symp = items.get(i);
				if (allSymptomsArray.contains(symp) == false) {
					doc.put("symptomName", symp);
					doc.put("symptomId", symp);
					symptomCollection.insert(doc);
				}
				allSymptomsArray = getAllSymptomsArray();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	

	/*
	 * Update patient details - Returns PatientId (same as original)
	 * ====================================================================
	 */

	public String updatePatient(String jsonPatient) {
		Date dob = null;
		Date doc = null;
		Date nextAppointment = null;
		String sdob, sdoc, snextAppointment;
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		BasicDBObject document = (BasicDBObject) JSON.parse(jsonPatient);
		String patid = document.get("PatientId").toString();

		try {
			// BasicDBObject document = (BasicDBObject) JSON.parse(jsonPatient);

			sdob = (String) document.get("dob");
			sdoc = (String) document.get("doc");
			

			if (sdob != null)
				dob = formatter.parse(sdob);
			if (sdoc != null)
				doc = formatter.parse(sdoc);
			
			document.remove("dob");
			document.remove("doc");			

			document.put("dob", dob);
			document.put("doc", doc);
			
			document.remove("PatientId");
			document.put("PatientId", new ObjectId(patid));
			
			
			DBObject update = new BasicDBObject("$set", document);
			DBObject query = new BasicDBObject("PatientId", new ObjectId(patid));

			patientdataCollection.updateMulti(query, update);

			String updatedPatId = "{\"PatientId\" : \""
					+ document.get("PatientId").toString() + "\"}";

			return updatedPatId;
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}

	}

	/*
	 * Get all patients information
	 * ====================================================================
	 */
	public String getAllPatients() {
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		JsonObject ret = new JsonObject();
		BasicDBObject whereQuery = new BasicDBObject();
		DBCursor cursor = patientdataCollection.find(whereQuery);
		JsonArray patArray = new JsonArray();
		JsonParser parser = new JsonParser();

		while (cursor.hasNext()) {
			DBObject dock = cursor.next();
			String sdock = dock.toString();
			// System.out.println(dock.get("_id").toString());

			Object odob = dock.get("dob");
			if (odob != null && odob instanceof Date == true) {
				Date ddob = (Date) odob;
				String sdob = formatter.format(ddob);
				dock.removeField("dob");
				dock.put("dob", sdob);

				LocalDate lddob = new LocalDate(ddob);
				LocalDate now = new LocalDate();
				Period period = new Period(lddob, now,
						PeriodType.yearMonthDay());
				int age = period.getYears();
				dock.put("age", age);
			}

			Object odoc = dock.get("doc");
			if (odoc != null && odoc instanceof Date == true) {
				Date ddoc = (Date) odoc;
				String sdoc = formatter.format(ddoc);
				dock.removeField("doc");
				dock.put("doc", sdoc);

				LocalDate lddoc = new LocalDate(ddoc);
				LocalDate now = new LocalDate();
				Period period = new Period(lddoc, now,
						PeriodType.yearMonthDay());
				int noOfMonths = (period.getYears()*12) + period.getMonths();
				dock.put("noOfMonths", noOfMonths);
			}

			Object onextAppointment = dock.get("nextAppointment");
			if (onextAppointment != null
					&& onextAppointment instanceof Date == true) {
				Date dnextAppointment = (Date) onextAppointment;
				String snextAppointment = formatter.format(dnextAppointment);
				dock.removeField("nextAppointment");
				dock.put("nextAppointment", snextAppointment);
			}

			List<BasicDBObject> newPrescription = editPrescriptionDateToString(sdock);
			dock.removeField("prescription");
			dock.put("prescription", newPrescription);

			String newsdock = dock.toString();
			JsonObject pat = (JsonObject) parser.parse(newsdock);
			patArray.add(pat);

		}

		ret.add("patients", patArray);
		System.out.println(ret.toString());
		return ret.toString();
	}

	public List<BasicDBObject> editPrescriptionDateToString(String dock) {		
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		List<BasicDBObject> prescriptionList = new ArrayList<BasicDBObject>();
		try {
			JSONObject jSONObject = new JSONObject(dock);
			JSONArray presArr = jSONObject.getJSONArray("prescription");
			for (int i = 0; i < presArr.length(); i++) {
				JSONObject anotherjsonObject = presArr.getJSONObject(i);
				String pres = anotherjsonObject.toString();

				BasicDBObject doc = (BasicDBObject) JSON.parse(pres);

				String drugName = (String) doc.get("drugName");
				String dosage = (String) doc.get("dosage");

				Object ostartDate = doc.get("startDate");
				Date dstartDate = (Date) ostartDate;
				String sstartDate = formatter.format(dstartDate);
				// System.out.println(sstartDate);

				Object oendDate = doc.get("endDate");
				Date dendDate = (Date) oendDate;
				String sendDate = formatter.format(dendDate);
				// System.out.println(sendDate);

				int time = anotherjsonObject.getInt("time");

				BasicDBObject document = new BasicDBObject();
				document.put("drugName", drugName);
				document.put("dosage", dosage);
				document.put("startDate", sstartDate);
				document.put("endDate", sendDate);
				document.put("time", time);
				prescriptionList.add(document);
			}

			return prescriptionList;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/*
	 * Get one patient's information, given the PatientId
	 * ====================================================================
	 */
	public String getOnePatient(String objId) {
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

		BasicDBObject whereQuery = new BasicDBObject();
		whereQuery.put("PatientId", new ObjectId(objId));
		DBCursor cursor = patientdataCollection.find(whereQuery);

		String val = "";

		if (cursor.hasNext()) {
			DBObject dock = cursor.next();

			Object odob = dock.get("dob");
			if (odob != null && odob instanceof Date == true) {
				Date ddob = (Date) odob;
				String sdob = formatter.format(ddob);
				dock.removeField("dob");
				dock.put("dob", sdob);

				LocalDate lddob = new LocalDate(ddob);
				LocalDate now = new LocalDate();
				Period period = new Period(lddob, now,
						PeriodType.yearMonthDay());
				int age = period.getYears();
				dock.put("age", age);
			}

			Object odoc = dock.get("doc");
			if (odoc != null && odoc instanceof Date == true) {
				Date ddoc = (Date) odoc;
				String sdoc = formatter.format(ddoc);
				dock.removeField("doc");
				dock.put("doc", sdoc);

				LocalDate lddoc = new LocalDate(ddoc);
				LocalDate now = new LocalDate();
				Period period = new Period(lddoc, now,
						PeriodType.yearMonthDay());
				int noOfMonths = period.getMonths();
				dock.put("noOfMonths", noOfMonths);
			}

			Object onextAppointment = dock.get("nextAppointment");
			if (onextAppointment != null
					&& onextAppointment instanceof Date == true) {
				Date dnextAppointment = (Date) onextAppointment;
				String snextAppointment = formatter.format(dnextAppointment);
				dock.removeField("nextAppointment");
				dock.put("nextAppointment", snextAppointment);
			}

			val = dock.toString();

			List<BasicDBObject> newPrescription = editPrescriptionDateToString(val);
			dock.removeField("prescription");
			dock.put("prescription", newPrescription);

			val = dock.toString();
		}

		System.out.println(val);
		return val;
	}

	/*
	 * Get appointments for today
	 * ====================================================================
	 */
	public String getTodaysAppointments() {
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		//formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
		JsonObject ret = new JsonObject();
		JsonArray patArray = new JsonArray();
		JsonParser parser = new JsonParser();

		DBObject condition = new BasicDBObject(2);

		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -1);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		condition.put("$gte", cal.getTime());

		cal.add(Calendar.DATE, 1);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		condition.put("$lt", cal.getTime());

		BasicDBObject whereQuery = new BasicDBObject();
		whereQuery.put("nextAppointment", condition);

		DBCursor cursor = patientdataCollection.find(whereQuery);
		while (cursor.hasNext()) {
			DBObject dock = cursor.next();
			// SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			// System.out.println("Today is " + sdf.format(dock.get("dob"))); //
			//String val = dock.toString();
			Object odoc = dock.get("doc");
			if (odoc != null && odoc instanceof Date == true) {
				Date ddoc = (Date) odoc;
				String sdoc = formatter.format(ddoc);
				dock.removeField("doc");
				dock.put("doc", sdoc);

				LocalDate lddoc = new LocalDate(ddoc);
				LocalDate now = new LocalDate();
				Period period = new Period(lddoc, now,
						PeriodType.yearMonthDay());
				int noOfMonths = (period.getYears()*12) + period.getMonths();
				dock.put("noOfMonths", noOfMonths);
			}
			JsonObject pat = (JsonObject) parser.parse(dock.toString());
			patArray.add(pat);
		}
		ret.add("patients", patArray);

		return ret.toString();
	}

	/*
	 * Get appointments for some date
	 * ====================================================================
	 */
	public String getDaysAppointments(String jsonday) {
		Date day = null;
		String sday;

		SimpleDateFormat formatter = new SimpleDateFormat(
				"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

		BasicDBObject document = (BasicDBObject) JSON.parse(jsonday);

		sday = (String) document.get("date");
		if (sday != null) {
			try {
				day = formatter.parse(sday.substring(0, 24));
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}

		JsonObject ret = new JsonObject();
		JsonArray patArray = new JsonArray();
		JsonParser parser = new JsonParser();

		DBObject condition = new BasicDBObject(2);

		Calendar cal = Calendar.getInstance();
		cal.setTime(day);
		cal.add(Calendar.DATE, 0);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		condition.put("$gte", cal.getTime());

		cal.add(Calendar.DATE, 1);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		condition.put("$lt", cal.getTime());
		BasicDBObject whereQuery = new BasicDBObject();
		whereQuery.put("nextAppointment", condition);

		DBCursor cursor = patientdataCollection.find(whereQuery);
		while (cursor.hasNext()) {
			DBObject dock = cursor.next();
			String val = dock.toString();
			JsonObject pat = (JsonObject) parser.parse(val);
			patArray.add(pat);
		}
		ret.add("patients", patArray);

		return ret.toString();
	}

	/*
	 * Get all medicines
	 * ====================================================================
	 */
	public String getAllMedicines() {
		JsonObject ret = new JsonObject();
		BasicDBObject whereQuery = new BasicDBObject();
		DBCursor cursor = medicationCollection.find(whereQuery);
		JsonArray medArray = new JsonArray();
		JsonParser parser = new JsonParser();

		while (cursor.hasNext()) {
			String val = cursor.next().toString();
			JsonObject med = (JsonObject) parser.parse(val);
			medArray.add(med);
		}
		ret.add("medicines", medArray);
		// System.out.println(ret.toString());

		return ret.toString();
	}
	
	/*
	 * Get all localities
	 * ====================================================================
	 */
	public String getAllLocalities() {
		JsonObject ret = new JsonObject();
		BasicDBObject whereQuery = new BasicDBObject();
		DBCursor cursor = localityCollection.find(whereQuery);
		JsonArray locArray = new JsonArray();
		JsonParser parser = new JsonParser();

		while (cursor.hasNext()) {
			String val = cursor.next().toString();
			JsonObject loc = (JsonObject) parser.parse(val);
			locArray.add(loc);
		}
		ret.add("locality", locArray);
		// System.out.println(ret.toString());

		return ret.toString();
	}	
	
	/*
	 * Get all symptoms
	 * ====================================================================
	 */
	public String getAllSymptoms() {
		JsonObject ret = new JsonObject();
		BasicDBObject whereQuery = new BasicDBObject();
		DBCursor cursor = symptomCollection.find(whereQuery);
		JsonArray sympArray = new JsonArray();
		JsonParser parser = new JsonParser();

		while (cursor.hasNext()) {
			String val = cursor.next().toString();
			JsonObject symp = (JsonObject) parser.parse(val);
			sympArray.add(symp);
		}
		ret.add("symptoms", sympArray);
		// System.out.println(ret.toString());

		return ret.toString();
	}	
	

	/*
	 * Get all medicines as an arrayList
	 * ====================================================================
	 */
	public ArrayList<String> getAllMedicinesArray() {
		String str = getAllMedicines();
		ArrayList<String> medicinesArray = new ArrayList<String>();

		try {
			JSONObject jSONObject = new JSONObject(str);
			JSONArray medArr = jSONObject.getJSONArray("medicines");
			for (int i = 0; i < medArr.length(); i++) {
				JSONObject anotherjsonObject = medArr.getJSONObject(i);
				// System.out.println(anotherjsonObject.getString("DrugName"));
				medicinesArray.add(anotherjsonObject.getString("drugName"));
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
		return medicinesArray;
	}
	
	
	/*
	 * Get all localities as an arrayList
	 * ====================================================================
	 */
	public ArrayList<String> getAllLocalityArray() {
		String str = getAllLocalities();
		ArrayList<String> locArray = new ArrayList<String>();		
		try{
			JSONObject jSONObject = new JSONObject(str);
			JSONArray locArr = jSONObject.getJSONArray("locality");
			for (int i = 0; i < locArr.length(); i++) {
				JSONObject anotherjsonObject = locArr.getJSONObject(i);
				//System.out.println(anotherjsonObject.getString("localityName"));
				locArray.add(anotherjsonObject.getString("localityName"));
			}			
		}catch(Exception e){
			e.printStackTrace();
		}
		return locArray;
	}
	
	
	/*
	 * Get all symptoms as an arrayList
	 * ====================================================================
	 */
	public ArrayList<String> getAllSymptomsArray() {
		String str = getAllSymptoms();
		ArrayList<String> sympArray = new ArrayList<String>();		
		try{
			JSONObject jSONObject = new JSONObject(str);
			JSONArray sympArr = jSONObject.getJSONArray("symptoms");
			for (int i = 0; i < sympArr.length(); i++) {
				JSONObject anotherjsonObject = sympArr.getJSONObject(i);
				//System.out.println(anotherjsonObject.getString("localityName"));
				sympArray.add(anotherjsonObject.getString("symptomName"));
			}			
		}catch(Exception e){
			e.printStackTrace();
		}
		return sympArray;
	}
	

	/*
	 * Get all notifications after a timestamp
	 * ====================================================================
	 */
	public String getNotifications(String timestamp) {
		long currentDateTime;
		DateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
		String fmttimestamp;
		Date input = null;
		JsonObject ret = new JsonObject();
		JsonArray notifArray = new JsonArray();
		JsonParser parser = new JsonParser();
		DBObject condition = new BasicDBObject();
		BasicDBObject whereQuery = new BasicDBObject();
		DBCursor cursor;
		DBCursor cursor2;
		BasicDBObject sortPredicate = new BasicDBObject();
		sortPredicate.put("date", 1);
		try {
			if (timestamp != null) {
				currentDateTime = Long.parseLong(timestamp);
				fmttimestamp = fmt.format(currentDateTime); 
				input = fmt.parse(fmttimestamp);
				System.out.println("Date passed: "+input);
				Calendar cal = Calendar.getInstance();
				cal.setTime(input);
				System.out.println("Cal: " + cal.getTime());
				condition.put("$gte", input);
				whereQuery.put("date", condition);
				cursor = ivrresponsesCollection.find(whereQuery).sort(sortPredicate);
			}			
			else
				cursor = ivrresponsesCollection.find().sort(sortPredicate);

			int numberOfNotifs = cursor.count();
			
			cursor2 = ivrresponsesCollection.find().sort(sortPredicate);
			while (cursor2.hasNext()) {				
				DBObject dock = cursor2.next();
				String val = dock.toString();
				JsonObject notif = (JsonObject) parser.parse(val);
				notifArray.add(notif);
			}
			ret.add("notifications", notifArray);
			BasicDBObject document = (BasicDBObject) JSON.parse(ret.toString());
			document.put("notifCount", numberOfNotifs);
			System.out.println("Output: " + document.toString());
			return document.toString();

		} catch (ParseException e) {
			e.printStackTrace();
			return "";
		}
	}

	/*
	 * Get all appointments for a patient
	 * ====================================================================
	 */
	public String getAllAppointments(String patId) {		
		//SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		BasicDBObject whereQuery = new BasicDBObject("PatientId", new ObjectId(patId));
		DBCursor cursor = lastVisitDatesCollection.find(whereQuery);
		
		DBObject dock = cursor.next();
		String sdock = dock.toString();
		//System.out.println(dock.get("appointmentInfo"));
		//List<BasicDBObject> newPrescription = editPrescriptionDateToString(sdock);		
		
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		List<BasicDBObject> prescriptionList = new ArrayList<BasicDBObject>();
		
		try {						
			JsonElement jelement = new JsonParser().parse(sdock);
		    JsonObject  jobject = jelement.getAsJsonObject();		    
		    JsonArray jarray = jobject.getAsJsonArray("appointmentInfo");
		    jobject = jarray.get(0).getAsJsonObject();		    
		    JsonArray presArr = jobject.getAsJsonArray("prescription");
		    
			for (int i = 0; i < presArr.size(); i++) {
				JsonObject anotherjsonObject = presArr.get(i).getAsJsonObject();
				String pres = anotherjsonObject.toString();

				BasicDBObject doc = (BasicDBObject) JSON.parse(pres);

				String drugName = (String) doc.get("drugName");
				String dosage = (String) doc.get("dosage");

				Object ostartDate = doc.get("startDate");
				Date dstartDate = (Date) ostartDate;
				String sstartDate = formatter.format(dstartDate);
				// System.out.println(sstartDate);

				Object oendDate = doc.get("endDate");
				Date dendDate = (Date) oendDate;
				String sendDate = formatter.format(dendDate);
				// System.out.println(sendDate);

				int time = anotherjsonObject.get("time").getAsInt();

				BasicDBObject document = new BasicDBObject();
				//JSONObject document = new JSONObject();				
				document.put("drugName", drugName);
				document.put("dosage", dosage);
				document.put("startDate", sstartDate);
				document.put("endDate", sendDate);
				document.put("time", time);
				prescriptionList.add(document);							
			}
			
			/*Object odate = jobject.get("appointmentDate");
			System.out.println(odate);
			Date date = (Date) odate;
			String sdate = formatter.format(date);
			
			dock.removeField("appointmentDate");
			dock.put("appointmentDate", sdate);*/
	    	
		}catch(Exception e){
			e.printStackTrace();
		}
		
		dock.removeField("prescription");
		dock.put("prescription", prescriptionList);
		
		Object onextAppointment = dock.get("nextAppointment");
		if (onextAppointment != null
				&& onextAppointment instanceof Date == true) {
			Date dnextAppointment = (Date) onextAppointment;
			String snextAppointment = formatter.format(dnextAppointment);
			dock.removeField("nextAppointment");
			dock.put("nextAppointment", snextAppointment);
		}
		
		System.out.println("getAllAppmnts: "+dock.toString());
		return dock.toString();
	}
	
	
	/*
	 * Get all records where response is 2 - currently NOT USED
	 * ====================================================================
	 */
	public String getRecords(){		
		JsonObject ret = new JsonObject();
		JsonArray patIdArray = new JsonArray();
		JsonParser parser = new JsonParser();
		
		BasicDBObject whereQuery = new BasicDBObject("response", "2");
		BasicDBObject fields = new BasicDBObject();
		fields.put("PatientId", 1);
		
		DBCursor cursor = ivrresponsesCollection.find(whereQuery);
		while (cursor.hasNext()) {
			DBObject dock = cursor.next();			
			String val = dock.toString();
			JsonObject patId = (JsonObject) parser.parse(val);
			patIdArray.add(patId);
		}
		ret.add("patients", patIdArray);

		return ret.toString();
	}
	
	/*
	 * Get response for patient - NOT TESTED
	 * ====================================================================
	 */
	public String getResponse(String patId){
		String response = null;
		DBObject condition = new BasicDBObject();
		BasicDBObject whereQuery = new BasicDBObject(2);
		
		whereQuery.put("PatientId", patId);
		
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.HOUR, -1);
		Date queryDate = cal.getTime();
		condition.put("$gte", queryDate);		
		whereQuery.put("timestamp", condition);
		
		DBCursor cursor = ivrresponsesCollection.find(whereQuery);
		if(cursor.hasNext()){
			DBObject dock = cursor.next();
			response = (String) dock.get("response");
		}		
		return response;
	}
	
	
	/*
	 * Search based on some criteria - EDIT nextAppointment part
	 * ====================================================================
	 */
	public String search(String criteria) {
		JsonObject ret = new JsonObject();
		JsonArray patIdArray = new JsonArray();
		JsonParser parser = new JsonParser();
		BasicDBObject whereQuery = new BasicDBObject();

		BasicDBObject document = (BasicDBObject) JSON.parse(criteria);
		if (document.containsField("nextAppointment")) {
			String sdate = document.getString("nextAppointment");			
			whereQuery.append("nextAppointment",
					StringToDate(sdate));
		}
		if (document.containsField("locality")) {
			whereQuery.append("locality", document.get("locality"));
		}
		if (document.containsField("symptoms")) {
			whereQuery.append("symptoms", document.get("symptoms"));
		}
		
		DBCursor cursor = patientdataCollection.find(whereQuery);
		while (cursor.hasNext()) {
			DBObject dock = cursor.next();
			String val = dock.toString();
			JsonObject patId = (JsonObject) parser.parse(val);
			patIdArray.add(patId);
		}
		ret.add("patients", patIdArray);
		//System.out.println(ret.toString());
		return ret.toString();
	}	
	
	
	public ArrayList<String> analyticsEngine(Date start, Date end){
		BasicDBObject document = new BasicDBObject();
		DBObject condition = new BasicDBObject(2);
		ArrayList<String> med = new ArrayList<String>();
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(start);
		cal.add(Calendar.DATE, 0);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		condition.put("$gte", cal.getTime());

		Calendar cal2 = Calendar.getInstance();
		cal2.setTime(end);
		cal2.add(Calendar.DATE, 1);
		cal2.set(Calendar.HOUR_OF_DAY, 0);
		cal2.set(Calendar.MINUTE, 0);
		cal2.set(Calendar.SECOND, 0);
		condition.put("$lte", cal2.getTime());
		
		BasicDBObject whereQuery = new BasicDBObject();
		whereQuery.put("date", condition);
		
		BasicDBObject fields = new BasicDBObject();
		fields.put("drugName", 1);

		DBCursor cursor = appmedmapCollection.find(whereQuery, fields);
		while(cursor.hasNext()){
			DBObject dock = cursor.next();
			System.out.println(dock.toString());			
			med.add(dock.get("drugName").toString());
		}		
		return med;
	}
	
	public String analytics(String jsonOfDates){
		BasicDBObject doc = (BasicDBObject) JSON.parse(jsonOfDates);
		String sstart = doc.getString("startDate");
		String send = doc.getString("endDate");
		int max = 0;
		String maxName = null;
		int maxCount = 0;
		BasicDBObject document = new BasicDBObject();
		Date start = StringToDate(sstart);
		Date end = StringToDate(send);
		ArrayList<String> retrievedMeds = new ArrayList<String>();
		ArrayList<String> allMedsArray = new ArrayList<String>();
		allMedsArray = getAllMedicinesArray();
		
		retrievedMeds = analyticsEngine(start, end);
		
		ArrayList<String> returnedString = new ArrayList<String>();
		
		for (String s : allMedsArray){
			int freq = Collections.frequency(retrievedMeds, s);
			//returnedString.add(arg0)
			document.put(s, freq);
			if(freq>max){
				maxName = s;
				maxCount = freq;
				max = freq;
			}
		}
		
		document.put("maxName", maxName);
		document.put("maxCount", maxCount);
		
		return document.toString();
	}

}