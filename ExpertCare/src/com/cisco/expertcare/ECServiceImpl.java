package com.cisco.expertcare;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import com.mongodb.DB;


@Path("/ECService")
public class ECServiceImpl {

	ECPatientDAO ecpatientdao;

	@POST
	@Path("/addAPatient")
	@Produces("application/json")
	public String addPatient(String jsonNewPatient) {		
		ECAction ecaction = new ECAction();
		DB ECDatabase = ecaction.initialize();
		ECPatientDAO ecpatientdao = new ECPatientDAO(ECDatabase);	
		
		try {	
			return ecpatientdao.addPatientDetails(jsonNewPatient);			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "Patient could not be added";
	}
	
	
	@POST
	@Path("/updateAPatient")
	@Produces("application/json")
	public String updatePatient(String jsonNewPatient) {		
		ECAction ecaction = new ECAction();
		DB ECDatabase = ecaction.initialize();
		ECPatientDAO ecpatientdao = new ECPatientDAO(ECDatabase);	
		
		try {	
			return ecpatientdao.updatePatient(jsonNewPatient);			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "Patient could not be updated";
	}
	
	
	@GET	
	@Path("/getAllPatients")
	@Produces("application/json")
	public String getPatients() {
		
		try {
			ECAction ecaction = new ECAction();
			DB ECDatabase = ecaction.initialize();
			ecpatientdao = new ECPatientDAO(ECDatabase);				
			return ecpatientdao.getAllPatients();			
		} catch (Exception e) {			
			e.printStackTrace();
		}
		return "error";
	}	
	
	
	@GET
	@Path("/getAPatient")
	@Produces("application/json")
	public String getAPatient(@QueryParam("id") String id){
		try {
			ECAction ecaction = new ECAction();
			DB ECDatabase = ecaction.initialize();
			ecpatientdao = new ECPatientDAO(ECDatabase);				
			return ecpatientdao.getOnePatient(id);			
		} catch (Exception e) {			
			e.printStackTrace();
		}
		return "error";
	}
	
	
	@GET
	@Path("/getAllAppmnts")
	@Produces("application/json")
	public String getAllAppmnts(@QueryParam("id") String id){
		try {
			ECAction ecaction = new ECAction();
			DB ECDatabase = ecaction.initialize();
			ecpatientdao = new ECPatientDAO(ECDatabase);				
			return ecpatientdao.getAllAppointments(id);			
		} catch (Exception e) {			
			e.printStackTrace();
		}
		return "error";
	}
	
	
	@GET	
	@Path("/getTodaysAppointments")
	@Produces("application/json")
	public String getTodAppointments() {
		
		try {
			ECAction ecaction = new ECAction();
			DB ECDatabase = ecaction.initialize();
			ecpatientdao = new ECPatientDAO(ECDatabase);				
			return ecpatientdao.getTodaysAppointments();			
		} catch (Exception e) {			
			e.printStackTrace();
		}
		return "error";
	}	
	
	@GET
	@Path("/getDaysAppointments")
	@Produces("application/json")
	public String getDayAppointments(String day) {
		
		try {
			ECAction ecaction = new ECAction();
			DB ECDatabase = ecaction.initialize();
			ecpatientdao = new ECPatientDAO(ECDatabase);				
			return ecpatientdao.getDaysAppointments(day);			
		} catch (Exception e) {			
			e.printStackTrace();
		}
		return "error";
	}	
	
	
	@GET
	@Path("/getAllMeds")
	@Produces("application/json")
	public String getMedicines() {
		
		try {
			ECAction ecaction = new ECAction();
			DB ECDatabase = ecaction.initialize();
			ecpatientdao = new ECPatientDAO(ECDatabase);				
			return ecpatientdao.getAllMedicines();			
		} catch (Exception e) {			
			e.printStackTrace();
		}
		return "error";
	}	
	
	@GET
	@Path("/getAllLocs")
	@Produces("application/json")
	public String getLocalities() {
		
		try {
			ECAction ecaction = new ECAction();
			DB ECDatabase = ecaction.initialize();
			ecpatientdao = new ECPatientDAO(ECDatabase);				
			return ecpatientdao.getAllLocalities();			
		} catch (Exception e) {			
			e.printStackTrace();
		}
		return "error";
	}	
	
	@GET
	@Path("/getAllSymps")
	@Produces("application/json")
	public String getSymptoms() {
		
		try {
			ECAction ecaction = new ECAction();
			DB ECDatabase = ecaction.initialize();
			ecpatientdao = new ECPatientDAO(ECDatabase);				
			return ecpatientdao.getAllSymptoms();			
		} catch (Exception e) {			
			e.printStackTrace();
		}
		return "error";
	}	
	
	
	@GET
	@Path("/getNotifs")
	@Produces("application/json")
	public String getNotifs(@QueryParam("lastNotif") String lastNotif) {		
		try {
			ECAction ecaction = new ECAction();
			DB ECDatabase = ecaction.initialize();
			ecpatientdao = new ECPatientDAO(ECDatabase);				
			return ecpatientdao.getNotifications(lastNotif);			
		} catch (Exception e) {			
			e.printStackTrace();
		}
		return "error";
	}
	
	
	@GET
	@Path("/search")
	@Produces("application/json")
	public String searchDB(String criteria) {		
		try {
			ECAction ecaction = new ECAction();
			DB ECDatabase = ecaction.initialize();
			ecpatientdao = new ECPatientDAO(ECDatabase);				
			return ecpatientdao.search(criteria);			
		} catch (Exception e) {			
			e.printStackTrace();
		}
		return "error";
	}
	

	@POST
	@Path("/addAppointment")
	@Produces("application/json")
	public String addAppointment(String jsonNewPatient) {		
		ECAction ecaction = new ECAction();
		DB ECDatabase = ecaction.initialize();
		ECPatientDAO ecpatientdao = new ECPatientDAO(ECDatabase);	
		
		try {	
			return ecpatientdao.addNewAppointment(jsonNewPatient);			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "Appointment could not be added";
	}
	
	
	@POST
	@Path("/doAnalytics")
	@Produces("application/json")
	public String doSomeCoolAnalytics(String jsonOfDates) {		
		ECAction ecaction = new ECAction();
		DB ECDatabase = ecaction.initialize();
		ECPatientDAO ecpatientdao = new ECPatientDAO(ECDatabase);	
		
		try {	
			return ecpatientdao.analytics(jsonOfDates);			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "Appointment could not be added";
	}
}