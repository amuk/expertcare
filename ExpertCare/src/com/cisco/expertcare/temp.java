package com.cisco.expertcare;

//import org.joda.time.format.ISODateTimeFormat;

/*import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.bson.types.ObjectId;*/

import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

import javax.xml.bind.DatatypeConverter;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.util.JSON;

import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.Period;
import org.joda.time.PeriodType;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class temp {

	public static void main(String[] args) {
		ECAction ecaction = new ECAction();

		DB ECDatabase = ecaction.initialize();

		ECPatientDAO ecpatientdao = new ECPatientDAO(ECDatabase);

		/*
		 * PatientDetails pat = new PatientDetails();
		 * 
		 * pat.setPatientID("000"); pat.setName("Unique Name");
		 * 
		 * SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy"); String dob
		 * = "31-08-1982"; String doc = "12-07-2014"; Date date1; Date date2;
		 * try { date1 = sdf.parse(dob); date2 = sdf.parse(doc);
		 * pat.setDob(date1); pat.setDoc(date2); } catch (Exception e) {
		 * e.printStackTrace(); }
		 * 
		 * pat.setContactNo("988034546"); pat.setAlternateNo("08023350464");
		 * 
		 * ArrayList<String> ailments = new ArrayList<String>(20);
		 * ailments.add("Diabetes"); ailments.add("BP");
		 * 
		 * pat.setAilments(ailments);
		 * 
		 * pat.setBloodGroup("B+");
		 * 
		 * ecpatientdao.addPatientDetails(pat);
		 */

		// getPatientsByAilment("Diabetes");

		// String s = getPatientID("Unique Name");
		// System.out.println(s);

		String str = "{" + "    \"firstName\": \"Jyothi\","
				+ "    \"lastName\": \"Prakash\","
				+ "    \"contactNo\": \"9999453232\","
				+ "    \"AltContactNo\": \"9999453232\","
				+ "    \"address\": \"xyz,13131\","
				+ "    \"symptoms\": \"low bp,giddiness\","
				+ "    \"dob\": \"2001-12-12T08:40:12.569Z\","
				+ "    \"doc\": \"2001-12-12T08:40:12.569Z\","
				+ "    \"locality\": \"Jayanagar\","
				+ "    \"bloodGroups\": \"B-\","
				+ "    \"nextAppointment\": \"2014-11-12T08:40:12.569Z\","
				+ "    \"prescription\": [" + "        {"
				+ "            \"drugName\": \"med2\","
				+ "            \"dosage\": \"500mg\","
				+ "            \"startDate\": \"2001-12-12T08:40:12.569Z\","
				+ "            \"endDate\": \"2001-12-12T08:40:12.569Z\","
				+ "            \"time\": 1" + "        }," + "        {"
				+ "            \"drugName\": \"med1\","
				+ "            \"dosage\": \"500mg\","
				+ "            \"startDate\": \"2001-12-12T08:40:12.569Z\","
				+ "            \"endDate\": \"2001-12-12T08:40:12.569Z\","
				+ "            \"time\": 1" + "        }" + "    ]" + "}";

		// ecpatientdao.addPatientDetails(str);
		// ecpatientdao.updateLastVisitInfo(str);

		String appmnt = "{"
				+ "    \"PatientId\": \"54693d93cd6644f219a1f22f\","
				+ "            \"appointmentDate\": \"2014-11-12T08:40:12.569Z\","
				+ "            \"symptoms\": \"Anaemia,Bodyache\","
				+ "            \"prescription\": ["
				+ "                {"
				+ "                    \"drugName\": \"Calcium\","
				+ "                    \"dosage\": \"20ml\","
				+ "                    \"startDate\": \"2010-12-12T08:40:12.569Z\","
				+ "                    \"endDate\": \"2010-12-12T08:40:12.569Z\","
				+ "                    \"time\": 2"
				+ "                },"
				+ "                {"
				+ "                    \"drugName\": \"Folic acid\","
				+ "                    \"dosage\": \"20ml\","
				+ "                    \"startDate\": \"2001-12-12T08:40:12.569Z\","
				+ "                    \"endDate\": \"2001-12-12T08:40:12.569Z\","
				+ "                    \"time\": 2" + "                }"
				+ "            ],"
				+ "            \"notes\": \"Regular Checkup\"" + "        }";

		
		String newstr = "{" + " \"nextAppointment\" : \"2014-11-18T08:40:12.569Z\","
				+ " \"locality\" : \"Marathahalli\"," + " }";

		//System.out.println(newstr);
		
		//System.out.println(ecpatientdao.search(newstr));
		
		//long currentDateTime = System.currentTimeMillis();
		/*long currentDateTime = Long.parseLong("1416238934922");
		DateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
		System.out.println(currentDateTime);
		
		System.out.println(fmt.format(currentDateTime));
	       //creating Date from millisecond
	       Date currentDate = new Date(currentDateTime);
	      
	       //printing value of Date
	       System.out.println("current Date: " + currentDate);
	      
	       String someDate = "2014-11-18T14:23:00.000Z";
	       DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
	       Date date = null;
	      long l = 0;
	       try {
			date = df.parse(someDate);
			l = date.getTime();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	       //formatted value of current Date
	       System.out.println("Milliseconds to Date: " + fmt.format(l));
	       
	       ecpatientdao.getNotifications(Long.toString(l));*/

		// ecpatientdao.addNewAppointment(appmnt);

		//ecpatientdao.getAllAppointments("546b87949888a23f3a99ac73");
		
		System.out.println(ecpatientdao.getAllMedicinesArray());
		
		Date s = null;
		Date e = null;
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		String ssdate = "01-01-2012";
		String sedate = "19-11-2012";
		try{
			s = sdf.parse(ssdate);
			e = sdf.parse(sedate);
			//System.out.println(ecpatientdao.analyticsEngine(s, e));
			//System.out.println(Collections.frequency(ecpatientdao.analyticsEngine(s, e), "Calcium tablets"));
		}catch(Exception exp){
			exp.printStackTrace();
		}
		
		//System.out.println(ecpatientdao.analytics(s, e));

		// ecpatientdao.addLocalityToDB(str);
		// System.out.println(ecpatientdao.getAllLocalityArray());

		// ecpatientdao.addSymptomsToDB(str);
		// System.out.println(ecpatientdao.getAllSymptomsArray());

		// ecpatientdao.getNotifications("11-Oct-2014 09:00:00");

		/*
		 * DBCollection patientdataCollection =
		 * ECDatabase.getCollection("patientdata"); BasicDBObject doc =
		 * (BasicDBObject) JSON.parse(appmnt);
		 * 
		 * SimpleDateFormat formatter = new
		 * SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"); String s =
		 * doc.getString("PatientId"); BasicDBObject whereQuery = new
		 * BasicDBObject("PatientId", new ObjectId(s)); DBCursor cursor =
		 * patientdataCollection.find(whereQuery);
		 * 
		 * DBObject document = cursor.next(); Object onextAppointment =
		 * document.get("nextAppointment"); Date dnextAppointment = (Date)
		 * onextAppointment; String snextAppointment =
		 * formatter.format(dnextAppointment); Date nextAppointment =
		 * StringToDate(snextAppointment);
		 * 
		 * String oappointmentDate = doc.getString("appointmentDate"); Date
		 * dappointmentDate = StringToDate(oappointmentDate); String
		 * sappointmentDate = formatter.format(dappointmentDate); Date
		 * appointmentDate = StringToDate(sappointmentDate);
		 * 
		 * Date today = new Date(); if(nextAppointment.before(today)){
		 * ecpatientdao.updateLastVisitDate(doc.getString("PatientId"),
		 * snextAppointment); } BasicDBObject newDocument = new BasicDBObject();
		 * newDocument.append("$set", new
		 * BasicDBObject().append("nextAppointment",
		 * appointmentDate).append("symptoms",
		 * doc.get("symptoms")).append("prescription",
		 * doc.get("prescription")));
		 * 
		 * patientdataCollection.update(whereQuery, newDocument);
		 */

		// Add new appointment
		/*
		 * DBCollection patientdataCollection =
		 * ECDatabase.getCollection("patientdata"); SimpleDateFormat formatter =
		 * new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		 * 
		 * BasicDBObject whereQuery = new BasicDBObject("PatientId", new
		 * ObjectId("545ecdf9cc82abf39d303382")); DBCursor cursor =
		 * patientdataCollection.find(whereQuery);
		 * 
		 * DBObject document = cursor.next();
		 * 
		 * Object onextAppointment = document.get("nextAppointment"); Date
		 * dnextAppointment = (Date) onextAppointment; String snextAppointment =
		 * formatter.format(dnextAppointment);
		 * System.out.println(snextAppointment); Date nextAppointment =
		 * StringToDate(snextAppointment); Date today = new Date(); Date
		 * newAppointment = StringToDate("2014-11-09T08:40:12.569Z");
		 * if(nextAppointment.before(today)){
		 * ecpatientdao.updateLastVisitDate("545ecdf9cc82abf39d303382",
		 * snextAppointment); } BasicDBObject newDocument = new BasicDBObject();
		 * newDocument.append("$set", new
		 * BasicDBObject().append("nextAppointment", newAppointment));
		 * 
		 * patientdataCollection.update(whereQuery, newDocument);
		 */

		// ecpatientdao.addPatientDetails(str);
		// System.out.println(new ObjectId("545ecf9dcc827cbf9db76182"));
		// ecpatientdao.updatePatient(str);

		// ecpatientdao.updateLastVisitDate("545ecf9dcc827cbf9db76182",
		// "2015-01-12T08:40:12.569Z");

		// ecpatientdao.getAllPatients();

		ecpatientdao.getOnePatient("545ecf9dcc827cbf9db76182");

		// ecpatientdao.getOnePatient("5454c71bcc82feb00bab3fa1");

		// System.out.println(ecpatientdao.StringToDate("2013-10-21T01:34:04.808Z"));

		// DateTime dateTime = new DateTime( "2013-10-21T01:34:04.808Z" );
		// ecpatientdao.getDoB("EK");

		// System.out.println(ISODate("2012-11-02T08:40:12.569Z"));

		// SimpleDateFormat df=new SimpleDateFormat("dd-MM-yyyy");
		// parse("2012-10-01T19:30:00Z");

		// ecpatientdao.getCurrentAppointments();

		/*
		 * String str = ecpatientdao.getAllMedicines(); System.out.println(str);
		 * 
		 * try { JSONObject jSONObject = new JSONObject(str); JSONArray
		 * featuresArr = jSONObject.getJSONArray("medicines"); for (int i=0;
		 * i<featuresArr.length(); i++){ JSONObject anotherjsonObject =
		 * featuresArr.getJSONObject(i);
		 * System.out.println(anotherjsonObject.getString("DrugName")); }
		 * 
		 * } catch (JSONException e) { // TODO Auto-generated catch block
		 * e.printStackTrace(); }
		 * 
		 * MongoClient mongoClient; try { mongoClient = new MongoClient(new
		 * MongoClientURI( "mongodb://localhost")); DB db =
		 * mongoClient.getDB("myDatabase"); DBCollection coll =
		 * db.getCollection("coll"); coll.drop(); ObjectId id = new ObjectId();
		 * DBObject obj = new BasicDBObject(); obj.put("_id", id);
		 * obj.put("title", "Blah!"); obj.put("body", "Blah Again!");
		 * coll.save(obj);
		 * 
		 * String idString = obj.get("_id").toString();
		 * System.out.println(idString); } catch (UnknownHostException e) { //
		 * TODO Auto-generated catch block e.printStackTrace(); }
		 */

	}	
	
	
	public static Date StringToDate(String sdate){
		SimpleDateFormat formatter;
		formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		Date date;
		try {
			date = formatter.parse(sdate.substring(0, 24));
			return date;
		} catch (ParseException e) {			
			e.printStackTrace();
			return null;
		}		
	}
	
	
	
	
	
	public static Date parse(final String str) {
        Calendar c = DatatypeConverter.parseDateTime(str);
        System.out.println(str + "\t" + (c.getTime().getTime()/1000));
        return c.getTime();
    }
	
	
	/*
	 // Get the PatientId of the patient whose name is given as input	 
	public String getPatientID(String name) {
		BasicDBObject whereQuery = new BasicDBObject();
		BasicDBObject fields = new BasicDBObject();
		fields.put("PatientId", 1);
		fields.put("_id", 0);
		whereQuery.put("name", name);
		DBCursor cursor = patientdataCollection.find(whereQuery, fields);

		return cursor.next().toString();
	}

	// Get all patients who have a particular ailment
	public List<String> getPatientsByAilment(String ailment) {
		List<String> patients = new ArrayList<String>();
		BasicDBObject whereQuery = new BasicDBObject();
		whereQuery.put("Ailment", ailment);
		BasicDBObject fields = new BasicDBObject();
		fields.put("Name", 1);
		fields.put("_id", 0);
		DBCursor cursor = patientdataCollection.find(whereQuery, fields);
		while (cursor.hasNext()) {
			patients.add(toName(cursor.next()));
			// System.out.println(cursor.next());
		}
		return patients;
	}
	
	public String toName(DBObject doc) {
		return (String) doc.get("Name");
	}
	
	// Get the names of all patients in the db
	public List<String> getAllPatientNames() {
		List<String> patients = new ArrayList<String>();
		BasicDBObject whereQuery = new BasicDBObject();
		BasicDBObject fields = new BasicDBObject();
		fields.put("Name", 1);
		fields.put("_id", 0);
		DBCursor cursor = patientdataCollection.find(whereQuery, fields);
		while (cursor.hasNext()) {
			patients.add(toName(cursor.next()));
			// System.out.println(cursor.next());
		}
		return patients;
	}*/
	

	/*public void getDoB(String name) {
		BasicDBObject whereQuery = new BasicDBObject();
		whereQuery.put("Name", name);
		BasicDBObject fields = new BasicDBObject();
		fields.put("DoC", 1);
		fields.put("_id", 0);
		DBCursor cursor = patientdataCollection.find(whereQuery, fields);

		DBObject dock = cursor.next();
		// DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");

		// String DATE_FORMAT = "dd/MM/yyyy";
		// SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
		// System.out.println("Today is " + sdf.format(dock.get("DoB"))); //

		System.out.println(dock.get("DoC"));
		System.out.println(new Date());
		System.out.println(getDateDiff((Date) dock.get("DoC"), new Date(),
				TimeUnit.DAYS));

	}

	public static long getDateDiff(Date date1, Date date2, TimeUnit timeUnit) {
		long diffInMillies = date2.getTime() - date1.getTime();
		return timeUnit.convert(diffInMillies, TimeUnit.MILLISECONDS);
	}*/

}