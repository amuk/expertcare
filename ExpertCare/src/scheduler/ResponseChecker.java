package scheduler;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

//import com.mkyong.quartz.HelloJob;
//import com.mkyong.quartz.ResponseFetch;

public class ResponseChecker {
	public void schedule(String patientId, String fName, String lName, String contact,List<String> startDate, List<String> endDate) throws Exception{	
		
		
		String begin, end;
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		Random randomGenerator = new Random();
		int id1 = randomGenerator.nextInt();
		int id2 = randomGenerator.nextInt();
		
		Iterator startIterator = startDate.iterator();
		Iterator stopIiterator = endDate.iterator();
		String id="1";
		int count =0;
		while(startIterator.hasNext() && stopIiterator.hasNext() ){
			
			JobDetail job = JobBuilder.newJob(ResponseFetch.class)
					.withIdentity("."+ id1, "."+ id2).build();
			
			job.getJobDataMap().put("patientId", patientId);
			job.getJobDataMap().put("fName", fName);
			job.getJobDataMap().put("lName", lName);
			job.getJobDataMap().put("contact", contact);
			id1 = randomGenerator.nextInt()*2;
			id2 = randomGenerator.nextInt();
			
			
			begin= (String) startIterator.next();
			end = (String) stopIiterator.next();
			Date s = formatter.parse(begin);
			Date e = formatter.parse(end);
			
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(s);
			int hour = calendar.get(Calendar.HOUR_OF_DAY);
			String cronExp = "";
			if(hour == 7){
				// It is a morning tablet
				cronExp ="0 25 7 * * ? *";
			}
			
			if(hour == 12){
				// It is an afternoon tablet
				cronExp ="0 25 12 * * ? *";
			}
			
			if(hour == 17){
				// It is an evening tablet
				cronExp ="0 25 17 * * ? *";
			}
			System.out.println("**************************");
			System.out.println("The cronExp is :"+ cronExp);
			System.out.println("The start date is :"+ s);
			System.out.println("The end date is:" + e);
			System.out.println("**************************");
			Trigger trigger = TriggerBuilder
					.newTrigger()
					.startAt(s)
					.endAt(e)
					.withIdentity(id1+".", id2+".")
					.withSchedule(
							/*SimpleScheduleBuilder.simpleSchedule()
							.withIntervalInMinutes(30)
							.withRepeatCount(2))*/
							CronScheduleBuilder.cronSchedule(cronExp))
					.build();
			
			// schedule it
			Scheduler scheduler = new StdSchedulerFactory().getScheduler();
			scheduler.start();
			scheduler.scheduleJob(job, trigger);
			
		
		}
		
		
	}
}
