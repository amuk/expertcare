package scheduler;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.GetMethod;
import org.bson.types.ObjectId;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;

//import SimpleTrigger.Reminder;

public class ResponseFetch implements Job{
	public void execute(JobExecutionContext context)
			throws JobExecutionException {
				// Fetch Response 
		JobDataMap data = context.getJobDetail().getJobDataMap();
		System.out.println(data.getString("patientId") + data.getString("fName") + data.getString("lName") + data.getString("contact"));
		//***************YOUR CODE STARTS HERE*************************//
		// Get the response for given patient Id corresponding to CURRENT HOUR & CURRENT DATE
		
		MongoClient mongoClient;
		DB ECDatabase = null;

		try {
			mongoClient = new MongoClient(new MongoClientURI(
					"mongodb://localhost"));
			ECDatabase = mongoClient.getDB("expertcare");	
			
			System.out.println("Initialized");			
		} catch (UnknownHostException e) {			
			e.printStackTrace();
		}
		DBCollection ivrresponsesCollection;
		ivrresponsesCollection = ECDatabase.getCollection("IVRresponse");
		String response = null;
		DBObject condition = new BasicDBObject();
		BasicDBObject whereQuery = new BasicDBObject(2);
		
		whereQuery.put("PatientId", new ObjectId(data.getString("patientId")));
		
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.HOUR, -1);
		Date queryDate = cal.getTime();
		condition.put("$gte", queryDate);		
		whereQuery.put("timestamp", condition);
		
		DBCursor cursor = ivrresponsesCollection.find(whereQuery);
		if(cursor.hasNext()){
			DBObject dock = cursor.next();
			response = (String) dock.get("response");
		}	
		
		System.out.println("Response: "+response);
		response = "2";
		
		
		// if response is 2 we give a call ONCE right NOW
		if(response.equals("2") ) {
		
			 System.out.println("Making at "+ new Date(System.currentTimeMillis()));
			 HttpClient client = new HttpClient();
			 String url = "http://173.39.242.206/twilio/gitHub_twilio/twilioReminder.php?patientId="+data.getString("patientId")+"&fName="+data.getString("fName")+"&lName="+data.getString("lName")+"&contact="+data.getString("contact");
			 System.out.println(url);
	          
	          // Create a method instance.
	          GetMethod method = new GetMethod(url);
	          
	          try {
	            // Execute the method.
	            int statusCode = client.executeMethod(method);

	            if (statusCode != HttpStatus.SC_OK) {
	              System.err.println("Method failed: " + method.getStatusLine());
	            }

	            // Read the response body.
	            byte[] responseBody = method.getResponseBody();

	            // Deal with the response.
	            // Use caution: ensure correct character encoding and is not binary data
	            System.out.println(new String(responseBody));

	          } catch (HttpException e) {
	            System.err.println("Fatal protocol violation: " + e.getMessage());
	            e.printStackTrace();
	          } catch (IOException e) {
	            System.err.println("Fatal transport error: " + e.getMessage());
	            e.printStackTrace();
	          } finally {
	            // Release the connection.
	            method.releaseConnection();
	          }  
		
	}
	else
		System.out.println("Response is not 2. Second call will not be placed.");
  }
		
}	
	
