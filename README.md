ExpertCare
==========

A high neonatal mortality rate exists in the rural villages of India due to a lack of nutritional and medical awareness among expecting mothers. We propose a system, ‘ExpertCare’, to address this. It comprises of a web interface for doctors to monitor an expecting mother’s well-being. IVR calls are generated to the phones of expecting mothers, as reminders of the prescribed medicines and to monitor any unusual symptoms. The recorded responses are monitored by the doctors. Nutritional and health awareness is thus made more accessible to rural expecting mothers, combating one of the major causes for neonatal mortality.
