package com.mkyong.quartz;

import java.io.IOException;
import java.util.Date;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.GetMethod;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class HelloJob implements Job
{
	public void execute(JobExecutionContext context)
	throws JobExecutionException {
		
		System.out.println("Creating JavaPhp object");
		JobDataMap data = context.getJobDetail().getJobDataMap();
		JavaPhp javaPhp = new JavaPhp();
		javaPhp.run(data.getString("patientId"),data.getString("fName"),data.getString("lName"),data.getString("contact"));
		System.out.println("Call made at "+ new Date(System.currentTimeMillis()));
	
	}
	
}
