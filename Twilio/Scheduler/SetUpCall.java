package com.mkyong.quartz;
import org.apache.commons.httpclient.*;
import org.apache.commons.httpclient.methods.*;
import org.apache.commons.httpclient.params.HttpMethodParams;

import SimpleTrigger.Details;
import SimpleTrigger.Reminder;
import SimpleTrigger.ResponseChecker;
import SimpleTrigger.SingleCall;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Random;
import java.io.*;
public class SetUpCall {

	public static void main(String[] args) throws Exception {
		//SimpleTrigger trigger = new SimpleTrigger();
		//trigger.schedule("1","Sam","0000000000",new Date(),new Date());
		Random randomGenerator = new Random();
		int id1 = randomGenerator.nextInt();
		int id2 = randomGenerator.nextInt();
		
		String a = "17-11-2014 17:30:00";
		String b = "17-11-2014 12:40:00";
		
		String a1 = "18-11-2014 19:30:00";
		String b1 = "18-11-2014 07:40:00";
		
		List<String> start = new ArrayList<String>();
		List<String> stop = new ArrayList<String>();
		
		start.add(a);
		start.add(b);
		
		stop.add(a1);
		stop.add(b1);
		
		String patientId="2";
		String contact="919962790287";
		String fname ="xyz";
		String lname= "qws";
		// Schedules call at x00 hrs everyday from start date to end date
		SingleCall trigger = new SingleCall();
		trigger.schedule(patientId, fname, lname, contact, start, stop,id1,id2);
		
                // checks response and places reminder call if needed
		ResponseChecker checkResponse = new ResponseChecker();
		checkResponse.schedule(patientId, fname, lname, contact, start, stop,id1++,id2--);
		
		
    }

}
