package SimpleTrigger;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;
import org.quartz.impl.triggers.CoreTrigger;

import com.mkyong.quartz.HelloJob;

public class SingleCall {
	public void schedule(String patientId, String fName, String lName, String contact,List<String> startDate, List<String> endDate, int id1, int id2) throws Exception{	
		JobDetail job = JobBuilder.newJob(HelloJob.class)
				.withIdentity("."+ id1, "."+ id2).build();
		
		job.getJobDataMap().put("patientId", patientId);
		job.getJobDataMap().put("fName", fName); 
		job.getJobDataMap().put("lName", lName);
		job.getJobDataMap().put("contact", contact);
		
		
		String begin, end;
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		
		
		Iterator startIterator = startDate.iterator();
		Iterator stopIiterator = endDate.iterator();
		String id="1";
		int count =0;
		while(startIterator.hasNext() && stopIiterator.hasNext() ){
			begin= (String) startIterator.next();
			end = (String) stopIiterator.next();
			Date s = formatter.parse(begin);
			Date e = formatter.parse(end);
			
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(s);
			int hour = calendar.get(Calendar.HOUR_OF_DAY);
			String cronExp = "";
			if(hour == 7){
				// It is a morning tablet
				cronExp ="0 0 7 * * ? *";
			}
			
			if(hour == 12){
				// It is an afternoon tablet
				cronExp ="0 0 12 * * ? *";
			}
			
			if(hour == 17){
				// It is an evening tablet
				cronExp ="0 0 17 * * ? *";
			}
			System.out.println("**************************");
			System.out.println("The cronExp is :"+ cronExp);
			System.out.println("The start date is :"+ s);
			System.out.println("The end date is:" + e);
			System.out.println("**************************");
			Trigger trigger = TriggerBuilder
					.newTrigger()
					.startAt(s)
					.endAt(e)
					.withIdentity(id, id)
					.withSchedule(
							/*SimpleScheduleBuilder.simpleSchedule()
							.withIntervalInMinutes(30)
							.withRepeatCount(2))*/
							CronScheduleBuilder.cronSchedule(cronExp))
					.build();
			
			// schedule it
			Scheduler scheduler = new StdSchedulerFactory().getScheduler();
			scheduler.start();
			scheduler.scheduleJob(job, trigger);
			
			id = id + count++;
			System.out.println(id);
		
		}
		
	}
}
